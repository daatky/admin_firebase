const express = require('express');
const router = express.Router();

const cliente_clientes = require('../controlador/cliente_clientes_controlador');
const clientes_controlador = new cliente_clientes();

const cliente_plantillas = require('../controlador/cliente_plantillas_controlador');
const plantillas_controlador = new cliente_plantillas();

var cliente_hotspots = require('../controlador/cliente_hotspots_controlador');
var hotspots_controlador = new cliente_hotspots();

var cliente_anuncios = require('../controlador/cliente_anuncios_controlador');
var anuncios_controlador = new cliente_anuncios();


/*Controladores*/
/*
 * Rutas para administrar Clientes 
*/
router.get('/clientes', clientes_controlador.administradorClientes);
router.get('/getContadoresPorCliente', clientes_controlador.getContadoresPorCliente);
router.post('/updateCampoCliente', clientes_controlador.updateCampoCliente);
router.post('/registrarSubCliente', clientes_controlador.registrarSubCliente);

/*
 * Rutas para administrar Maquetas 
*/
router.get('/maquetas', plantillas_controlador.administradorPlantillas);
router.post('/registrarMaqueta', plantillas_controlador.registrarMaqueta);
router.post('/updateCampoMaqueta', plantillas_controlador.updateCampoMaqueta);
router.post('/getDataModuloMaqueta', plantillas_controlador.getDataModuloMaqueta);
router.post('/agregarContenidoModulo', plantillas_controlador.agregarContenidoModulo);
router.post('/removeContenidoModulo', plantillas_controlador.removeContenidoModulo);

/* 
 * Rutas para administrar Hotspots
*/
router.get('/hotspots', hotspots_controlador.administradorHotspots);
router.post('/registrarHotspot', hotspots_controlador.registrarHotspot);
router.post('/updateCampoHotspot', hotspots_controlador.updateCampoHotspot);

/* 
 * Rutas para administrar Anuncios
*/
router.get('/anuncios', anuncios_controlador.administradorAnuncios);
router.post('/registrarAnuncio', anuncios_controlador.registrarAnuncio);
router.post('/updateCampoAnuncio', anuncios_controlador.updateCampoAnuncio);




module.exports = router;
