const express = require('express');
const router = express.Router();

const admin_clientes = require('../controlador/admin_clientes_controlador');
const clientes_controlador = new admin_clientes();

const admin_plantillas = require('../controlador/admin_plantillas_controlador');
const plantillas_controlador = new admin_plantillas();

var admin_hotspots = require('../controlador/admin_hotspots_controlador');
var hotspots_controlador = new admin_hotspots();

var admin_anuncios = require('../controlador/admin_anuncios_controlador');
var anuncios_controlador = new admin_anuncios();


/*Controladores*/
/*
 * Rutas para administrar Clientes 
*/
router.get('/clientes', clientes_controlador.administradorClientes);
router.get('/getContadoresPorCliente', clientes_controlador.getContadoresPorCliente);
router.post('/updateCampoCliente', clientes_controlador.updateCampoCliente);
router.post('/registrarCliente', clientes_controlador.registrarCliente);
router.post('/registrarSubCliente', clientes_controlador.registrarSubCliente);
router.post('/registrarProvincia', clientes_controlador.registrarProvincia);
router.post('/registrarCanton', clientes_controlador.registrarCanton);
router.post('/eliminarProvincia', clientes_controlador.eliminarProvincia);
router.post('/eliminarCanton', clientes_controlador.eliminarCanton);
router.post('/registrarPago', clientes_controlador.registrarPago);

/*
 * Rutas para administrar Maquetas 
*/
router.get('/maquetas', plantillas_controlador.administradorPlantillas);
router.post('/registrarMaqueta', plantillas_controlador.registrarMaqueta);
router.post('/updateCampoMaqueta', plantillas_controlador.updateCampoMaqueta);
router.post('/getDataModuloMaqueta', plantillas_controlador.getDataModuloMaqueta);
router.post('/agregarContenidoModulo', plantillas_controlador.agregarContenidoModulo);
router.post('/removeContenidoModulo', plantillas_controlador.removeContenidoModulo);

/* 
 * Rutas para administrar Hotspots
*/
router.get('/hotspots', hotspots_controlador.administradorHotspots);
router.post('/registrarHotspot', hotspots_controlador.registrarHotspot);
router.post('/updateCampoHotspot', hotspots_controlador.updateCampoHotspot);

/* 
 * Rutas para administrar Anuncios
*/
router.get('/anuncios', anuncios_controlador.administradorAnuncios);
router.post('/registrarAnuncio', anuncios_controlador.registrarAnuncio);
router.post('/updateCampoAnuncio', anuncios_controlador.updateCampoAnuncio);




module.exports = router;
