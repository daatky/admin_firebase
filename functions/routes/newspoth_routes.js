const express = require('express');
const router = express.Router();

const newspoth_control = require('../controlador/newspoth_controlador');
const newspoth = new newspoth_control();

router.post('/getDataAppGeneral', newspoth.getDataAppGeneral);
router.post('/addClick', newspoth.addClick);
router.post('/getDataModulo', newspoth.getDataModulo);
router.post('/getDataPuntosWifi', newspoth.getDataPuntosWifi);
router.post('/addUserEmailLikeButton', newspoth.addUserEmailLikeButton);


module.exports = router;