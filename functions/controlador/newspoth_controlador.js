const { admin, db, auth } = require('../config/firebase_config');
const FieldValue = admin.firestore.FieldValue;
const moment = require('moment-timezone');

class Newspoth {

    getDataAppGeneral(req,res){
        if(req.body.data) {
            let codigo = JSON.parse(req.body.data).codigo;
            db.collection('hotspot').where('codigo','==',codigo).limit(1).get()
                .then( equipos => {
                    let equipo = equipos.docs[0];
                    console.log(equipo.data());
                    let a = db.collection('clientes').doc(equipo.data().id_cliente).get();
                    let b = db.collection('maquetas').doc(equipo.data().id_maqueta).get();
                    let c = db.collection('anuncios').where('display','==','0').where('id_cliente', '==', equipo.data().id_cliente).get();
                    let d = db.collection('clientes').where('cuenta.estado','==',0).get();
                    let e = db.collection('hotspot').doc(equipo.id).get();
                    let f = db.collection('subcliente').where('id_cliente','==',equipo.data().id_cliente).get();
                    return Promise.all([a,b,c,d,e,f]);
                }).then( data => {
                    return res.json({
                        code: 0,
                        data: 'Operación exitosa',
                        d: {
                            equipo: {
                                id: data[4].id,
                                data: data[4].data()
                            },
                            cliente: {
                                id: data[0].id,
                                data: data[0].data()
                            },
                            maqueta: {
                                id: data[1].id,
                                data: data[1].data()
                            },
                            anuncios: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                            clientes: data[3].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                            subclientes: data[5].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                        }
                    });
                }).catch( error => {
                    console.log(error);
                    res.json({code:-1, data: 'Error, no se pudo completar la operación', lvl: 1});
                });
        } else {
            res.json({code:-1, data: 'Error, no tiene acceso a estas funciones'});
        }
    }

    addClick(req,res){
        if(req.body.data) {
            let d = JSON.parse(req.body.data);
            let clic = {
                id_anuncio: d.id_anuncio,
                id_equipo: d.id_equipo,
                id_cliente: d.id_cliente,
                id_subcliente: d.id_subcliente,
                dia: d.dia,
                mes: d.mes,
                ano: d.ano,
                hor: d.time.hora,
                min: d.time.minutos,
                seg: d.time.segundos,
            };

            let debito = {
                id_cliente: d.id_cliente,
                id_anuncio: d.id_anuncio,
                monto: 0.02,
                saldo: {
                    antes: 0,
                    despues: 0,
                },
                dia: d.dia,
                mes: d.mes,
                ano: d.ano,
                hor: d.time.hora,
                min: d.time.minutos,
                seg: d.time.segundos,
            };
            //FieldValue.increment(-50)
            db.collection('clientes').doc(d.id_cliente).get()
                .then( cliente => {
                    debito.saldo.antes = cliente.data().cuenta.saldo;
                    debito.saldo.despues = cliente.data().cuenta.saldo - 0.02;
                    return db.collection('clientes').doc(d.id_cliente).update({'cuenta.saldo': FieldValue.increment(-0.02)});
                }).then( ref => {
                    return db.collection('debitos').add(debito);
                }).then( deb => {
                    return db.collection('anuncios').doc(d.id_anuncio).update({'clicks.contador': FieldValue.increment(1)});
                }).then( ref2 => {
                    return db.collection('clics').add(clic);
                }).then( cli => {
                    return res.json({code: 0, data: 'Clic registrado'});
                }).catch( error => {
                    console.log(error);
                    return res.json({code: 0, data: 'Error, por favor recarga la página'});
                });
        } else {
            res.json({code:-1, data: 'Error, no tiene acceso a estas funciones'});
        }
    }

    getDataModulo(req,res){
        if(req.body.data) {
            let d = JSON.parse(req.body.data);
            db.collection('modulos').where('id_cliente','==',d.cliente).where('tipo','==',d.tipo).get()
                .then( modulo => {
                    if(modulo) {
                        let mod = modulo.docs[0];
                        return res.json({code: 0, data: 'Operación completada', modulo: { id: mod.id, data: mod.data() }});
                    } else {
                        return res.json({code: 0, data: 'Operación completada', modulo: { id: -1, data: null }});
                    }
                }).catch( error => {
                    console.log(error);
                    res.json({code:-1, data: 'Error al obtener la información'});
                });
        } else {
            res.json({code:-1, data: 'Error, no tiene acceso a estas funciones'});
        }
    }

    getDataPuntosWifi(req,res){
        if(req.body.data) {
            let d = JSON.parse(req.body.data);
            let a = db.collection('provincia').get();
            let b = db.collection('canton').get();
            let c = db.collection('hotspot').where('id_cliente','==',d.cliente).get()
            Promise.all([a,b,c]).then( data => {
                return res.json({
                    code: 0,
                    data: 'Operación exitosa',
                    provincias: data[0].docs.map( doc => { return {id: doc.id, data: doc.data()}; }),
                    cantones: data[1].docs.map( doc => { return {id: doc.id, data: doc.data()}; }),
                    equipos: data[2].docs.map( doc => { return {id: doc.id, data: doc.data()}; }),
                });
            }).catch( error => {
                console.log(error);
                res.json({code:-1, data: 'Error al obtener la información'});
            });
        } else {
            res.json({code:-1, data: 'Error, no tiene acceso a estas funciones'});
        }
    }

    addUserEmailLikeButton(req,res) {
        let d = JSON.parse(req.body.data);
        db.collection('users_fb').add({
            name: d.name,
            email: d.email,
            mac: d.mac,
            fecha: moment().tz("America/Guayaquil").format('L'),
            hora: moment().tz("America/Guayaquil").format('LTS')
        }).then( docRef => {
            return res.json({code: 0, data: 'Visita registrada'});
        }).catch( error => {
            console.log(error);
            res.json({code: -1, data: 'Error al registrar el email'});
        });
    }

}

module.exports = Newspoth;