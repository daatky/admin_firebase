
const moment = require('moment-timezone');
const { admin, db, auth } = require('../config/firebase_config');

class AdminPlantillasControlador {

    administradorPlantillas(req, res) {
        //let uid = 'A64eDqMRc5fupFE3sO634sFXiM62';
        let uid = req.query.a;
        if(req.query.a){
            let a = db.collection('clientes').doc(uid).get();
            let b = db.collection('maquetas').where('id_cliente','==',uid).get();
            Promise.all([a,b]).then(data => {
                return res.render('cliente/cliente_plantillas.hbs', {
                    user: {
                        id: data[0].id,
                        data: data[0].data()
                    },
                    lista_maquetas: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                });
            }).catch(error => {
                console.log(error);
                return res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }

    registrarMaqueta(req, res){
        let d = JSON.parse(req.body.data);
        d.fecha = moment().tz("America/Guayaquil").format('L');
        d.hora = moment().tz("America/Guayaquil").format('LTS'); 
        db.collection('maquetas').add(d)
            .then(doc => {
                return db.collection('maquetas').doc(doc.id).get();
            }).then(data => {
                return res.json({code: 0, data: 'Maqueta registrada', maqueta: {id: data.id, data: data.data()}});
            }).catch(error => {
                return res.json({code: -1, data: 'Error al registrar la maqueta'});
            });
    }

    updateCampoMaqueta(req,res) {
        let { id_maqueta, campo, tipo, old, valor } = JSON.parse(req.body.data);
        db.collection('maquetas').doc(id_maqueta).get()
            .then((maqueta) => {
                let d = maqueta.data();
                if(tipo === 0) {
                    d[campo] = valor;
                } else if(tipo === 1) {
                    d[campo.split('.')[0]][campo.split('.')[1]] = valor;
                } else if(tipo === 2){
                    d[campo.split('.')[0]][campo.split('.')[1]][campo.split('.')[2]] = valor;
                }
                return db.collection('maquetas').doc(id_maqueta).set(d);
            }).then((doc) => {
                return res.json({code:0, data:'Campo Actualizado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error, maqueta no registrada'});
            });
    }

    getDataModuloMaqueta(req,res) {
        let { id_cliente, button, tipo } = JSON.parse(req.body.data);
        let a = db.collection('modulos').where('id_cliente','==',id_cliente).where('tipo','==',tipo).limit(1).get();
        a.then(m => {
            return (m.docs.length <= 0) ? db.collection('modulos').add(JSON.parse(req.body.data)) : { code:0, data:'', modulo: {id: m.docs[0].id, data: m.docs[0].data()} };
        }).then(d => {
            return (typeof(d.code) === 'undefined') ? db.collection('modulos').doc(d.id).get() : d;
        }).then(a => {
            return (typeof(a.code) !== 'undefined') ? res.json(a) : res.json({ code: 0, data: '', modulo: {id: a.id, data: a.data()} });
        }).catch(error =>  {
            return res.json({code:-1, data:'Error al obtener la información'});
        });
    }

    agregarContenidoModulo(req,res) {
        let { id_modulo, tipo, item, nivel } = JSON.parse(req.body.data);
        let a = db.collection('modulos').doc(id_modulo).get();
        a.then(modulo => {
            let data = modulo.data();
            if(typeof(data.contenido) === 'undefined') {
                data.contenido = {
                    a: [],
                    b: [],
                    c: []
                };
            }

            if(nivel === 0) {
                item.id = getIdItem(data.contenido.a);
                data.contenido.a.push(item);
            } else if(nivel === 1) {
                item.id = getIdItem(data.contenido.b);
                data.contenido.b.push(item);
            } else if(nivel === 3) {
                item.id = getIdItem(data.contenido.c);
                data.contenido.c.push(item);
            }
            return db.collection('modulos').doc(id_modulo).set(data);
        }).then(() => {
            return db.collection('modulos').doc(id_modulo).get();
        }).then(doc => {
            return res.json({code:0, data:'Publicado correctamente', item: item});
        }).catch(error => {
            return res.json({code:-1, data:'Error, no se pudo completar la operación'});
        });
    }

    removeContenidoModulo(req,res) {
        let { id_modulo, id_item, nivel } = JSON.parse(req.body.data);
        let a = db.collection('modulos').doc(id_modulo).get();
        a.then(modulo => {
            let data = modulo.data();
            if(nivel === 0) {
                let aux = [];
                data.contenido.a.forEach( item => {
                    if(item.id !== id_item) {
                        aux.push(item);
                    }
                });
                data.contenido.a = aux;
            } else if(nivel === 1) {
                let aux = [];
                data.contenido.b.forEach( item => {
                    if(item.id !== id_item) {
                        aux.push(item);
                    }
                });
                data.contenido.b = aux;
            } else if(nivel === 3) {
                let aux = [];
                data.contenido.c.forEach( item => {
                    if(item.id !== id_item) {
                        aux.push(item);
                    }
                });
                data.contenido.c = aux;
            }
            return db.collection('modulos').doc(id_modulo).set(data);
        }).then(() => {
            return res.json({code:0, data:'Item Eliminado'});
        }).catch(error => {
            return res.json({code:-1, data:'Error, no se pudo completar la operación'});
        });
    }

}

module.exports = AdminPlantillasControlador;


function getIdItem(items){
    let aux;
    if(items.length > 0) {
        aux = items[0].id;
        items.forEach(item => {
            if(item.id > aux) {
                aux = item.id;
            }
        });
        aux += 1;
    } else {
        aux = 0;
    }
    return aux;
}