
const moment = require('moment-timezone');
const { admin, db, auth } = require('../config/firebase_config');

class AdminHotspotsControlador {

    administradorHotspots(req, res) {
        //let uid = 'A64eDqMRc5fupFE3sO634sFXiM62';
        let uid = req.query.a;
        if(req.query.a){
            let a = db.collection('clientes').doc(uid).get();
            let b = db.collection('clientes').where('tipo','==',1).get();
            let c = db.collection('provincia').orderBy('id_provincia', 'desc').get();
            let d = db.collection('canton').orderBy('id_provincia', 'desc').get();
            let e = db.collection('hotspot').orderBy('equipo.ip').get();
            let f = db.collection('maquetas').get();
            Promise.all([a,b,c,d,e,f]).then(data => {
                return res.render('admin/admin_hotspots.hbs', {
                    user: {
                        id: data[0].id,
                        data: data[0].data()
                    },
                    lista_clientes: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_provincias: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_cantones: data[3].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_hotspots: data[4].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_maquetas: data[5].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                });
            }).catch(error => {
                return res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }

    registrarHotspot(req, res) {
        let equipo = JSON.parse(req.body.data);
        db.collection('hotspot').orderBy('semilla','desc').limit(1).get()
            .then(s => {
                equipo.semilla = (s.empty) ? 0 : s.docs[0].data().semilla  + 1;
                equipo.codigo = getCode(equipo.semilla);
                return db.collection('hotspot').add(equipo);        
            }).then( e => {
                return db.collection('hotspot').doc(e.id).get();
            }).then( eq => {
                return res.json({code:0, data:'Equipo registrado', equipo: {id: eq.id, data: eq.data()}});
            }).catch( error => {
                return res.json({code:-1, data:'Error, no se pudo completar la operación'});
            });
    }

    updateCampoHotspot(req,res) {
        let { id_hotspot, campo, tipo, old, valor } = JSON.parse(req.body.data);
        db.collection('hotspot').doc(id_hotspot).get()
            .then((hotspot) => {
                let d = hotspot.data();
                if(tipo === 0) {
                    d[campo] = valor;
                } else if (tipo === 1) {
                    d[campo.split('.')[0]][campo.split('.')[1]] = valor;
                } else if(tipo === 2){
                    d[campo.split('.')[0]][campo.split('.')[1]][campo.split('.')[2]] = valor;
                }
                return db.collection('hotspot').doc(id_hotspot).set(d);
            }).then((doc) => {
                return res.json({code:0, data:'Campo Actualizado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error, maqueta no registrada'});
            });
    }
    

}

module.exports = AdminHotspotsControlador;

function RNG(seed) {
    // constantes de GCC
    this.m = 0x80000000; // 2**31;
    this.a = 1103515245;
    this.c = 12345;
    this.state = seed ? seed : Math.floor(Math.random() * (this.m - 1));
}

function getCode(semilla){
    RNG.prototype.nextInt = function() {
        this.state = (this.a * this.state + this.c) % this.m;
        return this.state;
    }
    RNG.prototype.nextFloat = function() {
        return this.nextInt() / (this.m - 1);
    }
    RNG.prototype.nextRange = function(start, end) {
        var rangeSize = end - start;
        var randomUnder1 = this.nextInt() / this.m;
        return start + Math.floor(randomUnder1 * rangeSize);
    }
    RNG.prototype.choice = function(array) {
        return array[this.nextRange(0, array.length)];
    }
    var rng = new RNG(semilla);
    var caracteres = "abcdefghijkmnpqrtuvwxyzABCDEFGHJKMNPQRTUVWXYZ2346789".split('');
    var codigo = "";
    for (var i = 0; i < 30; i++) {
        codigo += rng.choice(caracteres);
    }
    return codigo;
}