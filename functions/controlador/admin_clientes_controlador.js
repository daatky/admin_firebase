
const { admin, db, auth } = require('../config/firebase_config');

class AdminClientesControlador {

    administradorClientes(req, res) {
        //let uid = 'A64eDqMRc5fupFE3sO634sFXiM62';
        let uid = req.query.a;
        if(req.query.a){
            let a = db.collection('clientes').doc(uid).get();
            let b = db.collection('clientes').where('tipo','==',1).get();
            let c = db.collection('subcliente').get();
            let d = db.collection('provincia').orderBy('id_provincia', 'desc').get();
            let e = db.collection('canton').orderBy('id_provincia', 'desc').get();
            let f = db.collection('pagos').orderBy('anno').orderBy('mes').orderBy('dia').get();
            Promise.all([a,b,c,d,e,f]).then(data => {
                console.log(data[0].data());
                console.log({
                    id: data[0].id,
                    data: data[0].data()
                });
                return res.render('admin/admin_clientes.hbs', {
                    user: {
                        id: data[0].id,
                        data: data[0].data()
                    },
                    lista_clientes: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_subclientes: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_provincias: data[3].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_cantones: data[4].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_pagos: data[5].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                });
            }).catch(error => {
                console.log(error);
                return res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }

    getContadoresPorCliente(req,res) {
        let id = req.query.id;
        let anuncios = db.collection('publicaciones').where('id_cliente','==',id).get();
        let hotspots = db.collection('hotspots').where('id_cliente','==',id).get();
        let plantillas = db.collection('maquetas').where('id_cliente','==',id).get();
        Promise.all([anuncios, hotspots, plantillas]).then(data => {
            return res.json({
                code: 0,
                anuncios: data[0].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
                hotspots: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
                plantillas: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
            });
        }).catch(error => {
            return res.json({code: -1, data: 'Error al obtener las estadísticas generales del cliente'});
        });
    }

    updateCampoCliente(req,res) {
        db.collection('clientes').doc(req.body.cliente).get()
            .then((cliente) => {
                console.log(req.body);
                let d = cliente.data();
                if(req.body.tipo === '0') {
                    d[req.body.campo] = req.body.new;
                } else if(req.body.tipo === '1') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]] = req.body.new;
                } else if(req.body.tipo === '2') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]]['tipo'] = parseInt(req.body.new.split('/')[0]);
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]]['etiqueta'] = req.body.new.split('/')[1];
                } else if(req.body.tipo === '3') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]] = parseInt(req.body.new);
                }
                return db.collection('clientes').doc(req.body.cliente).set(d);
            }).then((doc) => {
                return res.json({code:0, data:'Campo Actualizado'});
            }).catch(error => {
                console.log(error);
                return res.json({code:-1, data:'Error, cliente no registrado'});
            });
    }

    registrarCliente(req,res) {
        let data = JSON.parse(req.body.data);
        data.password = data.nombres.split('')[0] + data.apellidos.split('')[0] + data.email.split('@')[0] + '@hotspot';
        auth.createUser({
            email: data.email,
            password: data.password
        }).then(u => {
           return db.collection('clientes').doc(u.uid).set(data); 
        }).then(cliente => {
            return db.collection('clientes').where('email','==',data.email).where('password','==',data.password).limit(1).get();
        }).then(cli => {
            return res.json({code:0, data:'Cliente Registrado', cliente: {id: cli.docs[0].id, data: cli.docs[0].data()}});
        }).catch(error => {
            var errorCode = error.code;
            var errorMessage = error.message;
            if(errorCode === 'auth/email-already-in-use'){
                return res.json({code: -1, data: 'La dirección de email ingresada ya se encuentra registrada'});
            }else if(errorCode === 'auth/invalid-email'){
                return res.json({code: -1, data: 'La dirección de email ingresada es invalida'});
            } else {
                return res.json({code:-1, data:'Error al registrar el cliente'});
            }
        });
    }

    registrarSubCliente(req,res) {
        db.collection('subcliente').add(JSON.parse(req.body.data))
            .then(subcliente => {
                return db.collection('subcliente').doc(subcliente.id).get();
            }).then(sub => {
                return res.json({code:0, data:'Sub-Cliente Registrado', subcliente: {id: sub.id, data: sub.data()}});
            }).catch(error => {
                return res.json({code:-1, data:'Error al registrar subcliente'});
            });
    }

    registrarProvincia(req,res) {
        db.collection('provincia').add(JSON.parse(req.body.data))
            .then(provincia => {
                return db.collection('provincia').doc(provincia.id).get();
            }).then(doc => {
                return res.json({code:0, data:'Provincia Registrada', provincia: {id: doc.id, data: doc.data()}});
            }).catch(error => {
                return res.json({code:-1, data:'Error al registrar la provincia'});
            });
    }

    registrarCanton(req,res) {
        db.collection('canton').add(JSON.parse(req.body.data))
            .then(canton => {
                return db.collection('canton').doc(canton.id).get();
            }).then(doc => {
                return res.json({code:0, data:'Cantón Registrado', canton: {id: doc.id, data: doc.data()}});
            }).catch(error => {
                return res.json({code:-1, data:'Error al registrar el cantón'});
            });
    }

    eliminarProvincia(req,res) {
        db.collection('provincia').doc(req.body.id_provincia).delete()
            .then(()=>{
                return db.collection('canton').where('id_provincia','==',req.body.codigo).get();
            }).then(cantones => {
                return cantones.forEach(doc => {
                    doc.ref.delete();
                });
            }).then(doc => {
                return res.json({code:0, data:'Provincia eliminada'});
            }).catch(error => {
                return res.json({code:-1, data:'Error al eliminar la provincia'});
            });
    }

    eliminarCanton(req,res) {
        db.collection('canton').doc(req.body.id_canton).delete()
            .then(()=>{
                return res.json({code:0, data:'Cantón eliminado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error al eliminar el cantón'});
            });
    }

    updateEstadoCliente(req,res) {
        db.collection('cliente').doc(req.body.id_canton).delete()
            .then(()=>{
                return res.json({code:0, data:'Cantón eliminado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error al eliminar el cantón'});
            });
    }

    registrarPago(req,res) {
        let data = JSON.parse(req.body.data);
        db.collection('pagos').add(JSON.parse(req.body.data))
            .then(pago => {
                let a = db.collection('pagos').doc(pago.id).get();
                let b = db.collection('clientes').doc(data.id_cliente).get(); 
                return Promise.all([a,b]);
            }).then(result => {
                let pago = result[0];
                let cliente = result[1];
                let a = db.collection('clientes').doc(data.id_cliente).update({'cuenta.saldo': pago.data().monto + cliente.data().cuenta.saldo}); 
                return {
                    pago: {
                        id: pago.id,
                        data: pago.data()
                    },
                    saldo_new: pago.data().monto + cliente.data().cuenta.saldo,
                    update: a
                };
            }).then(resultb => {
                return res.json({code:0, data:'Pago registrado', pago: resultb.pago, saldo_new: resultb.saldo_new});
            })
            .catch(error => {
                return res.json({code:-1, data:'Error al registrar el pago'});
            });
    }

}

module.exports = AdminClientesControlador;
