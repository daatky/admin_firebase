
const { admin, db, auth } = require('../config/firebase_config');

class AdminClientesControlador {

    administradorClientes(req, res) {
        //let uid = 'A64eDqMRc5fupFE3sO634sFXiM62';
        let uid = req.query.a;
        if(req.query.a){
            let a = db.collection('clientes').doc(uid).get();
            let b = db.collection('subcliente').where('id_cliente','==',uid).get();
            let c = db.collection('pagos').orderBy('anno').orderBy('mes').orderBy('dia').get();
            Promise.all([a,b,c]).then(data => {
                return res.render('cliente/cliente_clientes.hbs', {
                    user: {
                        id: data[0].id,
                        data: data[0].data()
                    },
                    lista_subclientes: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_pagos: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                });
            }).catch(error => {
                console.log(error);
                return res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }

    getContadoresPorCliente(req,res) {
        let id = req.query.id;
        let anuncios = db.collection('publicaciones').where('id_cliente','==',id).get();
        let hotspots = db.collection('hotspots').where('id_cliente','==',id).get();
        let plantillas = db.collection('maquetas').where('id_cliente','==',id).get();
        Promise.all([anuncios, hotspots, plantillas]).then(data => {
            return res.json({
                code: 0,
                anuncios: data[0].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
                hotspots: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
                plantillas: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}).length,
            });
        }).catch(error => {
            return res.json({code: -1, data: 'Error al obtener las estadísticas generales del cliente'});
        });
    }

    updateCampoCliente(req,res) {
        db.collection('subcliente').doc(req.body.cliente).get()
            .then((cliente) => {
                let d = cliente.data();
                if(req.body.tipo === '0') {
                    d[req.body.campo] = req.body.new;
                } else if(req.body.tipo === '1') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]] = req.body.new;
                } else if(req.body.tipo === '2') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]]['tipo'] = parseInt(req.body.new.split('/')[0]);
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]]['etiqueta'] = req.body.new.split('/')[1];
                } else if(req.body.tipo === '3') {
                    d[req.body.campo.split('.')[0]][req.body.campo.split('.')[1]] = parseInt(req.body.new);
                }
                return db.collection('subcliente').doc(req.body.cliente).set(d);
            }).then((doc) => {
                return res.json({code:0, data:'Campo Actualizado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error, sub-cliente no registrado'});
            });
    }

    registrarSubCliente(req,res) {
        db.collection('subcliente').add(JSON.parse(req.body.data))
            .then(subcliente => {
                return db.collection('subcliente').doc(subcliente.id).get();
            }).then(sub => {
                return res.json({code:0, data:'Sub-Cliente Registrado', subcliente: {id: sub.id, data: sub.data()}});
            }).catch(error => {
                return res.json({code:-1, data:'Error al registrar subcliente'});
            });
    }

}

module.exports = AdminClientesControlador;
