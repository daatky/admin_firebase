
const moment = require('moment-timezone');
const { admin, db, auth } = require('../config/firebase_config');

class AdminHotspotsControlador {

    administradorAnuncios(req, res) {
        //let uid = 'A64eDqMRc5fupFE3sO634sFXiM62';
        let uid = req.query.a;
        if(req.query.a){
            let a = db.collection('clientes').doc(uid).get();
            let b = db.collection('clientes').where('tipo','==',1).get();
            let c = db.collection('provincia').orderBy('id_provincia', 'desc').get();
            let d = db.collection('canton').orderBy('id_provincia', 'desc').get();
            let e = db.collection('hotspot').orderBy('equipo.ip').get();
            let f = db.collection('anuncios').get();
            let g = db.collection('anuncios_indices').get();
            let h = db.collection('subcliente').get();
            Promise.all([a,b,c,d,e,f,g, h]).then(data => {
                return res.render('admin/admin_anuncios.hbs', {
                    user: {
                        id: data[0].id,
                        data: data[0].data()
                    },
                    lista_clientes: data[1].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_provincias: data[2].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_cantones: data[3].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_hotspots: data[4].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_anuncios: data[5].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_anuncios_indices: data[6].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                    lista_subclientes: data[7].docs.map((doc) => {return {id: doc.id, data: doc.data()};}),
                });
            }).catch(error => {
                return res.redirect('/');
            });
        }else{
            res.redirect('/');
        }
    }

    registrarAnuncio(req, res) {
        let anuncio = JSON.parse(req.body.data);

        db.collection('anuncios').add(anuncio)
            .then(a => {
                return db.collection('anuncios').doc(a.id).get();
            }).then(aa => {
                return res.json({code:0, data:'Anuncio registrado', anuncio: {id: aa.id, data: aa.data()}});
            }).catch(error => {
                return res.json({code:-1, data:'Error, no se pudo completar la operación'});
            });
    }

    updateCampoAnuncio(req,res) {
        let { id_anuncio, campo, tipo, old, valor } = JSON.parse(req.body.data);
        db.collection('anuncios').doc(id_anuncio).get()
            .then((anuncio) => {
                let d = anuncio.data();
                if(tipo === 0) {
                    d[campo] = valor;
                } else if (tipo === 1) {
                    d[campo.split('.')[0]][campo.split('.')[1]] = valor;
                } else if(tipo === 2){
                    let i = JSON.parse(valor);
                    d[campo] = i.info;
                }
                return db.collection('anuncios').doc(id_anuncio).set(d);
            }).then((doc) => {
                return res.json({code:0, data:'Campo Actualizado'});
            }).catch(error => {
                return res.json({code:-1, data:'Error, maqueta no registrada'});
            });
    }

}

module.exports = AdminHotspotsControlador;