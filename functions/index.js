/* eslint-disable prefer-arrow-callback */
/* eslint-disable promise/no-nesting */
/* eslint-disable promise/always-return */
const functions = require('firebase-functions');
const express = require('express');
const http = require('http');
const cors = require('cors');
const bodyParser = require('body-parser');
const { admin, db, auth } = require('./config/firebase_config');
const router = express.Router();
const moment = require('moment-timezone');

const app = express();
app.set('views', './views')
    .set('view engine', 'hbs')
    .set('view engine', 'pug')
    .use(cors({ origin: true })) // Automatically allow cross-origin requests
    .use(bodyParser.json())
    .use(bodyParser.urlencoded({
        extended: true
    }))
    .set('trust proxy', 1)
    .use((req, res, next) => {
        res.setHeader('Access-Control-Allow-Origin', '*');
        res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
        res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');
        res.setHeader('Access-Control-Allow-Credentials', false);
        next();
    });

var hbs = require('hbs');
hbs.registerHelper('json',function(obj) {
	return new hbs.SafeString(JSON.stringify(obj))
});

hbs.registerHelper('comparar', function (v1, operator, v2, options) {
    switch (operator) {
        case '==':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '===':
            return (v1 === v2) ? options.fn(this) : options.inverse(this);
        case '!=':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '!==':
            return (v1 !== v2) ? options.fn(this) : options.inverse(this);
        case '<':
            return (v1 < v2) ? options.fn(this) : options.inverse(this);
        case '<=':
            return (v1 <= v2) ? options.fn(this) : options.inverse(this);
        case '>':
            return (v1 > v2) ? options.fn(this) : options.inverse(this);
        case '>=':
            return (v1 >= v2) ? options.fn(this) : options.inverse(this);
        case '&&':
            return (v1 && v2) ? options.fn(this) : options.inverse(this);
        case '||':
            return (v1 || v2) ? options.fn(this) : options.inverse(this);
        default:
            return options.inverse(this);
    }
});


/*Routes*/
var admin_routes = require('./routes/admin_routes');
app.use('/admin', admin_routes);

var cliente_routes = require('./routes/cliente_routes');
app.use('/cliente', cliente_routes);

var newspoth_routes = require('./routes/newspoth_routes');
app.use('/newspoth', newspoth_routes);


/*Update code*/

router.get('/', (req, res) => {
    res.render('index.hbs');
});

router.get('/master.clientes', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/clientes.pug');
});

router.get('/update.clientes', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('update/clientes.pug');
});

router.get('/master.plantillas', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/plantillas.pug');
});

router.get('/master.hotspots', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/hotspots.pug');
});

router.get('/master.publicaciones', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/publicaciones.pug');
});

router.get('/master.promociones', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/promociones.pug');
});

router.get('/master.redessociales', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/redessociales.pug');
});

router.get('/master.infoturismo', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/infoturismo.pug');
});

router.get('/master.reportes', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('master/reportes.pug');
});

router.get('/index', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('newspoth/main.pug', {data_equipo:{
        mac: req.query.mac,
        macesc: req.query.macesc,
        linkloginonly: req.query.linkloginonly,
        linkorigesc: req.query.linkorigesc,
        codigo: req.query.codigo,
        error: req.query.error,
        trial: req.query.trial
    }});
});

router.get('/main', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('newspoth/main.pug', {data_equipo:{
        mac: req.query.mac,
        macesc: req.query.macesc,
        linkloginonly: req.query.linkloginonly,
        linkorigesc: req.query.linkorigesc,
        codigo: req.query.codigo,
        error: req.query.error,
        trial: req.query.trial
    }});
});

router.get('/index_prueba', (req, res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    res.render('newspoth/index.pug', {data_equipo:{
        mac: req.query.mac,
        macesc: req.query.macesc,
        linkloginonly: req.query.linkloginonly,
        linkorigesc: req.query.linkorigesc,
        codigo: req.query.codigo,
        error: req.query.error,
        trial: req.query.trial
    }});
});

router.get('/getPlantilla', (req,res) => {
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    if(req.query.codigo && req.query.codigo.length>0){
        db.collection('hotspots').where('codigo', '==', req.query.codigo).get().then(equipos=>{
            if(!equipos.empty){
                //Data de la plantilla
                db.collection('plantillas').doc(equipos.docs[0].data().id_plantilla).get().then(plantilla=>{
                    if(!plantilla.empty){
                        res.json({code:0, data:'', plantilla:plantilla.data()});
                    }else{
                        res.json({code:-2, data:'Lo sentimos, ocurrio un error'});        
                        //A futuro crear una plantilla por default
                    }
                }).catch(error=>{
                    res.json({code:-2, data:'Lo sentimos, ocurrio un error', err:error});        
                });
            }else{
                res.json({code:-1, data:'Error, equipo no registrado'});
            }
        }).catch(error=>{
            res.json({code:-2, data:'Lo sentimos, ocurrio un error &#128533;', err:error});
        });
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/getPub', (req,res) => {
    if(req.query.codigo && req.query.codigo.length>0){
        db.collection('hotspots').where('codigo', '==', req.query.codigo).get().then(equipos=>{
            if(!equipos.empty){
                let e = equipos.docs[0];
                //Data de la plantilla
                //Donde id_hotspots contiene id_equipo
                //Donde el id_canton del equipo esta contenido en el id_canton de la publicacion
                //Si id_hotspot es igual a all, filtrar por id_publicacion y id_canton
                db.collection('publicaciones').where('id_hotspots','array-contains',e.id).get().then(result_a=>{
                    let aux = [];
                    result_a.forEach((pub_a) => {
                        let a = {
                            id: pub_a.id,
                            data: pub_a.data()
                        };
                        aux.push(a);
                    });
                    
                    // eslint-disable-next-line promise/always-return
                    db.collection('publicaciones').where('id_hotspots','array-contains','all').get().then(result_b=>{
                        result_b.forEach((pub_b) => {
                            if(pub_b.data().id_provincia.indexOf(e.data().id_provincia)>=0 && pub_b.data().id_canton.indexOf(e.data().id_canton)>=0){
                                let b = {
                                    id: pub_b.id,
                                    data: pub_b.data()
                                };
                                aux.push(b);
                            }
                        });
                        res.json({code:0, data:'', pubs:aux});
                    }).catch(error=>{
                        res.json({code:-2, data:'Lo sentimos, ocurrio un error', err:error});            
                    });
                }).catch(error=>{
                    res.json({code:-2, data:'Lo sentimos, ocurrio un error', err:error});        
                });
            }else{
                res.json({code:-1, data:'Error, equipo no registrado'});
            }
        }).catch(error=>{
            res.json({code:-2, data:'Lo sentimos, ocurrio un error &#128533;', err:error});
        });
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/addClick', (req,res) => {
    if(req.query.codigo && req.query.codigo.length>0){
        if(req.query.id && req.query.id.length>0){
            db.collection('hotspots').where('codigo', '==', req.query.codigo).get().then(equipos=>{
                if(!equipos.empty){
                    db.collection('publicaciones').doc(req.query.id).get().then(pub=>{
                        if(!pub.empty){
                            db.collection('publicaciones').doc(pub.id).update({
                                "visualizaciones.contador_visual": pub.data().visualizaciones.contador_visual + 1
                            }).then(() => {
                                //Falta controlar las alarmar cuando se ha alcanzado los procentajes de 50 80 de visual
                                let equipo = equipos.docs[0];
                                let mod = {
                                    id_equipo: equipo.id,
                                    id_publicacion: pub.id,
                                    fecha: moment().tz("America/Guayaquil").format('L'),
                                    hora: moment().tz("America/Guayaquil").format('LTS')
                                };
                                db.collection('clicks').add(mod).then(docRef=>{
                                    res.json({code:0, data:'Visualización registrada'});
                                }).catch(error=>{
                                    res.json({code:-2, data:'Lo sentimos, ocurrio un error', err:error});        
                                });
                            }).catch((error) => {
                                res.json({code:-1, data:'Error, no existen coincidencias'});      
                            });
                        }else{
                            res.json({code:-1, data:'Error, no existen coincidencias'});  
                        }
                    }).catch(error=>{
                        res.json({code:-1, data:'Error, no existen coincidencias'});  
                    });
                }else{
                    res.json({code:-1, data:'Error, equipo no registrado'});
                }
            }).catch(error=>{
                res.json({code:-1, data:'Error, no existen coincidencias'});  
            });
        }else{
            res.json({code:-1, data:'Error, no existen coincidencias'});    
        }
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/getPromos', (req,res)=>{
    if(req.query.codigo && req.query.codigo.length>0){
        db.collection('hotspots').where('codigo', '==', req.query.codigo).get().then(equipos=>{
            let data = {};
            if(!equipos.empty){
                db.collection('promociones').orderBy("fecha", "desc").limit(20).get().then(promos=>{
                    let p = [];
                    promos.forEach(promo => {
                        p.push({
                            id:promo.id,
                            data:promo.data()
                        });
                    });
                    res.json({code:0, data:'Consulta exitosa', promos:p});
                }).catch(error=>{
                    res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar las promociones'});  
                });
            }else{
                res.json({code:-1, data:'Error, equipo no registrado'});
            }
        }).catch(error=>{
            res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar las promociones'});
        });
    }else{
        return res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/getSocial', (req,res)=>{
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    if(req.query.codigo && req.query.codigo.length>0){
        if(req.query.mac && req.query.mac.length>0){
            db.collection('hotspots').where('codigo', '==', req.query.codigo).get().then(equipos=>{
                if(!equipos.empty){
                    db.collection('socialmedia').where('tipo','==',0).get().then(socials=>{
                        let p = [];
                        socials.forEach(social=>{
                            p.push({id:social.id,data:social.data()});
                        });
                        res.json({code:0, data:'Consulta exitosa', social:p});
                    }).catch(error=>{
                        res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar el contenido', error:error});  
                    });
                }else{
                    res.json({code:-1, data:'Error, equipo no registrado'});
                }
            }).catch(error=>{
                res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar el contenido', error:error});
            });
        }else{
            res.json({code:-1, data:'Error, la mac del cliente es necesaria'});        
        }
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/getPuntosWifi', (req,res)=>{
    res.set('Cache-Control', 'public, max-age=300, s-maxage=600');
    if(req.query.codigo && req.query.codigo.length>0){
        if(req.query.mac && req.query.mac.length>0){
            db.collection('provincia').orderBy("etiqueta").get().then(pro=>{
                let p_aux = [];
                pro.forEach(a => {
                    p_aux.push({
                        id:a.id,
                        data:a.data()
                    });
                });
                db.collection('hotspots').get().then(puntos=>{
                    let p = [];
                    puntos.forEach(punto => {
                        p.push({
                            id:punto.id,
                            data:punto.data()
                        });
                    });
                    res.json({code:0, data:'Consulta exitosa', provincias:p_aux, puntos_wifi:p});
                }).catch(error=>{
                    res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar el contenido', error:error});
                });
            }).catch(error=>{
                res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar el contenido', error:error});
            });
        }else{
            res.json({code:-1, data:'Error, la mac del cliente es necesaria'});        
        }
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.get('/getInfoTurismo', (req,res)=>{
    if(req.query.codigo && req.query.codigo.length>0){
        if(req.query.mac && req.query.mac.length>0){
            db.collection('info_turismo').get().then(infos=>{
                let p = [];
                infos.forEach(info => {
                    p.push({
                        id:info.id,
                        data:info.data()
                    });
                });
                res.json({code:0, data:'Consulta exitosa', info_turismo:p});
            }).catch(error=>{
                res.json({code:-1, data:'Lo sentimos, ocurrio un error al cargar el contenido', error:error});
            });
        }else{
            res.json({code:-1, data:'Error, la mac del cliente es necesaria'});        
        }
    }else{
        res.json({code:-1, data:'Error, equipo no registrado'});
    }
});

router.post('/addUserEmail', (req,res) => {
    db.collection('users_email').add({
        email: req.body.email,
        edad: req.body.edad,
        genero: req.body.genero,
        mac: req.body.mac,
        fecha: moment().tz("America/Guayaquil").format('L'),
        hora: moment().tz("America/Guayaquil").format('LTS')
    }).then(function(docRef){
        res.json({code: 0, data: 'Email registrado'});
    }).catch(function(error){
        console.log(error);
        res.json({code: -1, data: 'Error al registrar el email'});
    });
});

router.post('/addUserEmailLikeButton', (req,res) => {
    db.collection('users_fb').add({
        name: req.body.name,
        email: req.body.email,
        mac: req.body.mac,
        fecha: moment().tz("America/Guayaquil").format('L'),
        hora: moment().tz("America/Guayaquil").format('LTS')
    }).then(function(docRef){
        res.json({code: 0, data: 'Email registrado'});
    }).catch(function(error){
        console.log(error);
        res.json({code: -1, data: 'Error al registrar el email'});
    });
});

app.use(router);

exports.app = functions.https.onRequest(app);
