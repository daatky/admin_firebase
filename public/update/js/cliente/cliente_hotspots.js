$(document).ready(function(){
    $('.tabs').tabs();
    $('.collapsible').collapsible();

    const api_url = 'http://localhost:4000';
    //const api_url = 'https://newspoth.com';
    var  status_proceso = false;

    var config = {
        apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
        authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
        databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
        projectId: "hotspotmagazine-bf2eb",
        storageBucket: "hotspotmagazine-bf2eb.appspot.com",
        messagingSenderId: "282938551360"
    };
    firebase.initializeApp(config);
    const auth = firebase.auth();

    auth.onAuthStateChanged(function (user) {
        if (user) {
            console.log(user);
        } else {
            window.location.href = "/";
        }
    });

    //Evento de cambio en los input del formulario
    //$('#info_empresa').find('input:text').val('');

    function motrarIndicadorProgreso(status){
        if(status) {
            status_proceso = true;
            $('#indicador_progreso').addClass('active');
        } else {
            status_proceso = false;
            $('#indicador_progreso').removeClass('active');
        }
    }

    function motrarIndicadorProgresoID(id, status){
        if(status)
            $('#'+id).addClass('active');
        else
            $('#'+id).removeClass('active');
    }

    function cambiarEstadoBotonModal(id, status){
        if(status) {
            $('#'+id).addClass('noactive');
        } else {
            $('#'+id).removeClass('noactive');
        }
    }

    function mostrarMensajeDeError(jqXHR, textStatus, errorThrown){
        //Validar error y dar control de errores : status
        if (jqXHR.status === 0) {
            //alert('Not connect: Verify Network.');
            M.toast({html: 'Error, verifica tu conexión a internet'})
        } else if (jqXHR.status == 404) {
            //alert('Requested page not found [404]');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (jqXHR.status == 500) {
            //alert('Internal Server Error [500].');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'parsererror') {
            //alert('Requested JSON parse failed.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'timeout') {
            //alert('Time out error.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'abort') {
            //alert('Ajax request aborted.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else {
            //alert('Uncaught Error: ' + jqXHR.responseText);
            M.toast({html: 'No se ha podido completar la operación'})
        }
    }

    $('.btn_modal_action').click(function(){
        let modal = $(this).attr('data-target');
        $('#'+modal).addClass('active');
    });

    $('.btn_close_modale').click(function(){
        let modal = $(this).attr('data-target');
        $('#'+modal).removeClass('active');
        $('#'+modal).find('input').val('');
        $('.select_clean').val('none');
    });

    function setMesajeToast(msg){
        M.toast({html: msg, classes: 'rounded'});
    }

    function setErrorOInfo(id, msg, tipo){
        $('#'+id).text(msg);
        if(tipo === 0) {
            $('#'+id).removeClass('info');
            $('#'+id).addClass('error');
        } else  if(tipo === 1){
            $('#'+id).removeClass('error');
            $('#'+id).addClass('info');
        } else if(tipo === -1){
            $('#'+id).removeClass('error');
            $('#'+id).removeClass('info');
        }
    }

    function mostrarLoader(id, show){
        if(show) {
            $('#'+id).addClass('active');
        } else {
            $('#'+id).removeClass('active');
        }
    }

    function peticionAjaxServidor(d, type, dataType, url, cb) {
        $.ajax({
            url: url,
            type: type,
            cache: false,
            dataType: dataType,
            data: {
                data: JSON.stringify(d)
            }
        }).done(function (d) {
            cb(d);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
            cb({code: -2, data: ''});
        });
    }

    function setValorCampoTipoA(id, val, dt) {
        $('#'+id).val(val);
        $('#'+id).attr('placeholder', val);
        $('#'+id).attr('data-target', dt);
    }

    function updateCampoHotspot(d){
        lista_hotspots.forEach(h =>  {
            if(h.id === d.id_hotspot) {
                if(d.tipo === 0) {
                    h.data[d.campo] = d.valor;
                } else if(d.tipo === 1){
                    h.data[d.campo.split('.')[0]][d.campo.split('.')[1]] = d.valor;
                } else if(d.tipo === 2) {
                    h.data[d.campo.split('.')[0]][d.campo.split('.')[1]][d.campo.split('.')[2]] = d.valor;
                }
            }
        });
    }

    /**
     * Hacia arriba 
     * Común para todos los scripts, revisar ciertos métodos para modificar funcionalidad general
    */

    function cambiarCantonesPorProvincia(select){
        let id_provincia = select.val();
        let select_canton = '#'+select.attr('data-canton');
        $(select_canton).attr('disabled', true);
        if(id_provincia !== 'none') {
            $(select_canton).empty();
            $(select_canton).append('<option value="none" selected>¿En qué cantón esta ubicado?</option>');
            lista_cantones.forEach( canton => {
                if( canton.data.id_provincia === id_provincia ) {
                    $(select_canton).append('<option value="'+canton.id+'">'+canton.data.etiqueta+'</option>');
                }
            });
            $(select_canton).removeAttr('disabled');
        }
    }

    $('.select_provincia').change(function(){
        cambiarCantonesPorProvincia($(this));
    });

    function validarCamposHotspot(){
        let res = false;
        if( !($('#reg_hot_id_cliente').val() !== 'none') ) {
            M.toast({html: 'Error, debes seleccionar el cliente'});  
        } else if( !($('#reg_hot_id_maqueta').val() !== 'none') ) {
            M.toast({html: 'Error, debes seleccionar la maqueta'});  
        } else if( !($('#reg_hot_id_provincia').val() !== 'none') ) {
            M.toast({html: 'Error, debes seleccionar la provincia'});  
        } else if( !($('#reg_hot_id_canton').val() !== 'none') ) {
            M.toast({html: 'Error, debes seleccionar el cantón'});  
        } else if( !($('#reg_hot_lugar_nombre').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar el nombre del establecimiento o lugar'});  
        } else if( !($('#reg_hot_lugar_direccion').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la dirección del establecimiento o lugar'});  
        } else if( !($('#reg_hot_lugar_telefono').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar el teléfono del establecimiento o lugar'});  
        } else if( !($('#reg_hot_equipo_ip').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la IP del equipo'});  
        } else if( !($('#reg_hot_equipo_usuario').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar el usuario FTP del equipo'});  
        } else if( !($('#reg_hot_equipo_password').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la contraseña del usuario FTP del equipo'});  
        } else {
            res = true;
        } 
        return res;
    }

    function insertarHotspotAListaHTML(equipo) {
        let html  = '<li class="li_item" id="li_hotspot_'+equipo.id+'" data-target="'+equipo.id+'">';
            html += '   <div class="vertical">';
            html += '       <i class="fas fa-briefcase"></i>';
            html += '       <span class="maqueta_etiqueta">'+equipo.data.lugar.nombre+'</span>';
            html += '   </div>';
            html += '</li>';
        
        $('#ul_maquetas_lista').append(html);
    }

    $('#registrar_hotspot').click(function(){
        if ( validarCamposHotspot() ) {
            motrarIndicadorProgresoID('modale_progreso_add_hotspot', true);
            cambiarEstadoBotonModal('registrar_hotspot', true);
            let d = {
                id_cliente: $('#reg_hot_id_cliente').val(),
                id_maqueta: $('#reg_hot_id_maqueta').val(),
                id_provincia: $('#reg_hot_id_provincia').val(),
                id_canton: $('#reg_hot_id_canton').val(),
                lugar: {
                    nombre: $('#reg_hot_lugar_nombre').val(),
                    direccion: $('#reg_hot_lugar_direccion').val(),
                    telefono: $('#reg_hot_lugar_telefono').val(),
                },
                equipo: {
                    ip: $('#reg_hot_equipo_ip').val(),
                    ftp_user: $('#reg_hot_equipo_usuario').val(),
                    ftp_password: $('#reg_hot_equipo_password').val(),
                },
                semilla: -1,
                codigo: '',
            };
            peticionAjaxServidor(d, 'POST', 'json', '/admin/registrarHotspot', data => {
                if(data.code !== -2) {
                    M.toast({html: data.data});
                    if(data.code === 0) {
                        $('#modal_add_hotspot').find('input').val('');
                        $('.select_clean').val('none');
                        lista_hotspots.push(data.equipo);
                        insertarHotspotAListaHTML(data.equipo);
                    }
                }
                motrarIndicadorProgresoID('modale_progreso_add_hotspot', false);
                cambiarEstadoBotonModal('registrar_hotspot', false);
            });
        }
    });
    
    $('body').on('click','.li_item', function(){
        $('.li_item').removeClass('active');
        $(this).addClass('active');
        let id_hotspot = $(this).attr('data-target');
        if(! status_proceso ) {
            motrarIndicadorProgreso(true);
            lista_hotspots.forEach(eq => {
                if(id_hotspot === eq.id) {
                    let { id_cliente, id_maqueta, id_provincia, id_canton, lugar, equipo, semilla, codigo } = eq.data;
                    
                    //Limpiar campos
                    $('#form_hotspot_data').find('input').val('');
                    $('#form_hotspot_data').find('select').val('none');

                    $('#id_hotspot').val(id_hotspot);
                    setValorCampoTipoA('id_cliente', id_cliente, id_hotspot+'|id_cliente|0');
                    setValorCampoTipoA('id_maqueta', id_maqueta, id_hotspot+'|id_maqueta|0');
                    setValorCampoTipoA('id_provincia', id_provincia, id_hotspot+'|id_provincia|0');
                    cambiarCantonesPorProvincia($('#id_provincia'));

                    setValorCampoTipoA('id_canton', id_canton, id_hotspot+'|id_canton|0');

                    setValorCampoTipoA('lugar_nombre', lugar.nombre, id_hotspot+'|lugar.nombre|1');
                    setValorCampoTipoA('lugar_direccion', lugar.direccion, id_hotspot+'|lugar.direccion|1');
                    setValorCampoTipoA('lugar_telefono', lugar.telefono, id_hotspot+'|lugar.telefono|1');

                    setValorCampoTipoA('semilla', semilla, id_hotspot+'|semilla|0');
                    setValorCampoTipoA('codigo', codigo, id_hotspot+'|codigo|0');

                    setValorCampoTipoA('equipo_ip', equipo.ip, id_hotspot+'|equipo.ip|1');
                    setValorCampoTipoA('ftp_user', equipo.ftp_user, id_hotspot+'|equipo.ftp_user|1');
                    setValorCampoTipoA('ftp_password', equipo.ftp_password, id_hotspot+'|equipo.ftp_password|1');

                    motrarIndicadorProgreso(false);
                }
            });
        }else{
            M.toast({html: 'Existe otro proceso en ejecución'});
        }
    });

    $('.select_hotspot').change(function(){
        if($('#id_hotspot').val().length > 0) {
            if(!status_proceso) {
                let input = $(this);
                if( (input.val() !== 'none') && (input.val() !== input.attr('placeholder'))) {
                    motrarIndicadorProgreso(true);
                    let d = {
                        id_campo: input.attr('id'),
                        id_hotspot: input.attr('data-target').split('|')[0],
                        campo: input.attr('data-target').split('|')[1],
                        tipo: parseInt(input.attr('data-target').split('|')[2]),
                        old: input.attr('placeholder'),
                        valor: input.val()
                    };
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoHotspot', (data_b) => {
                        if(data_b.code !== -2)
                            M.toast({html: data_b.data});

                        if(data_b.code === 0) {
                            updateCampoHotspot(d);
                            setValorCampoTipoA(d.id_campo, d.valor, d.id_hotspot+'|'+d.campo+'|'+d.tipo);
                        }
                        motrarIndicadorProgreso(false);
                    });
                } else {
                    input.val(input.attr('placeholder'));
                }
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });

    $('.input_hotspot').keypress(function (e) {
		if(e.which ==13){
			if($(this).val().length <= 0){
				if($(this).attr('data-target').length > 0){
					$(this).val($(this).attr('placeholder'));
				}
			} else {
				if($(this).attr('data-target').length > 0){
					motrarIndicadorProgreso(true);
                    //Guardar Cambio y actualizar
                    let input = $(this);
					let d = {
                        id_campo: input.attr('id'),
                        id_hotspot: input.attr('data-target').split('|')[0],
                        campo: input.attr('data-target').split('|')[1],
                        tipo: parseInt(input.attr('data-target').split('|')[2]),
                        old: input.attr('placeholder'),
                        valor: input.val()
					};
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoHotspot', (data_b) => {
                        if(data_b.code !== -2)
                            M.toast({html: data_b.data});

                        if(data_b.code === 0) {
                            updateCampoHotspot(d);
                            setValorCampoTipoA(d.id_campo, d.valor, d.id_hotspot+'|'+d.campo+'|'+d.tipo);
                        }
                        motrarIndicadorProgreso(false);
                    });
				}
			}
		}
    });

    const ip_apinode = user.data.urls.api_node;
    
    $('#btn_update_file_hotspot').click(function(){
        let id_equipo = $('#upd_file_hotspot').val();
        console.log(id_equipo);
        if(id_equipo != null) {
            motrarIndicadorProgresoID('modale_progreso_update_file_hotspot', true);
            cambiarEstadoBotonModal('btn_update_file_hotspot', true);
            cambiarEstadoBotonModal('btn_restore_file_hotspot', true);
            
            let e;
            lista_hotspots.forEach(equipo => {
                if(id_equipo === equipo.id) {
                    e = {
                        id: equipo.id,
                        codigo: equipo.data.codigo,
                        equipo_ip: equipo.data.equipo.ip,
                        equipo_user: equipo.data.equipo.ftp_user,
                        equipo_password: equipo.data.equipo.ftp_password, 
                        url_red: user.data.urls.url_red,
                    };
                }    
            });

            peticionAjaxServidor(e, 'POST', 'json', ip_apinode+'/ftp/changeLoginFileOnly', data => {
                console.log(data);
                if(data.code !== -2){
                    if (data.mensaje) {
                        M.toast({html: data.mensaje});
                    } else if(data.data) {
                        M.toast({html: data.data});
                    }
                }

                if(data.code === 0) {
                    
                }

                motrarIndicadorProgresoID('modale_progreso_update_file_hotspot', false);
                cambiarEstadoBotonModal('btn_update_file_hotspot', false);
                cambiarEstadoBotonModal('btn_restore_file_hotspot', false);
            });
        } else {
            M.toast({html: 'Error, debes seleccionar al meno un equipo', displayLength: 900});
        }
    });

    $('#btn_restore_file_hotspot').click(function(){
        let equipo = $('#upd_file_hotspot').val();
        if( equipo != null ){
            motrarIndicadorProgresoID('modale_progreso_update_file_hotspot', true);
            cambiarEstadoBotonModal('btn_restore_file_hotspot', true);
            cambiarEstadoBotonModal('btn_update_file_hotspot', true);
            
            let e;
            lista_hotspots.forEach(equipo => {
                if(id_equipo === equipo.id) {
                    e = {
                        id: equipo.id,
                        codigo: equipo.data.codigo,
                        equipo_ip: equipo.data.equipo.ip,
                        equipo_user: equipo.data.equipo.ftp_user,
                        equipo_password: equipo.data.equipo.ftp_password, 
                    };
                }    
            });
            
            peticionAjaxServidor(e, 'POST', 'json', ip_apinode+'/ftp/restoreDefaultHotspotFiles', (data) => {
                console.log(data);
                if(data.code !== -2)
                    M.toast({html: 'Error al actualizar el archivo'});

                if(data.code === 0) {
                    
                }

                motrarIndicadorProgresoID('modale_progreso_update_file_hotspot', false);
                cambiarEstadoBotonModal('btn_restore_file_hotspot', false);
                cambiarEstadoBotonModal('btn_update_file_hotspot', false);
            });
        }else{
            M.toast({html: 'Error, debes seleccionar el hotspot', classes: 'rounded'});
        }
    });

});