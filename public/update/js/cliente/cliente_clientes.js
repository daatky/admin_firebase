$(document).ready(function () {
	$('.tabs').tabs();
	$('.collapsible').collapsible();

	var  status_proceso = false;

	var config = {
		apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
		authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
		databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
		projectId: "hotspotmagazine-bf2eb",
		storageBucket: "hotspotmagazine-bf2eb.appspot.com",
		messagingSenderId: "282938551360"
	};
	firebase.initializeApp(config);
	const auth = firebase.auth();

	auth.onAuthStateChanged(function (user) {
		if (user) {
			console.log(user);
		} else {
			window.location.href = "/";
		}
	});

	asignarContadores();

	function asignarContadores() {
		let cliente = user;
		$.ajax({
			url: '/admin/getContadoresPorCliente',
			type: "GET",
			cache: false,
			dataType:'json',
			data: { id: cliente.id }
		}).done(function (data) {
			if(data.code == 0) {
				let iterator = 0;
				let totales = [data.anuncios, data.hotspots, data.plantillas];
				$('.contador').each(function () {
					var $this = $(this);
					let total = totales[iterator];
					iterator += 1;
					console.log(totales);
					$({ counter: 0 }).animate({ counter: ''+total }, {
					  duration: 1500,
					  easing: 'swing',
					  step: function () {
						$this.text(Math.ceil(this.counter));
					  }
					});
				});
				$('#cliente_saldo').text( parseFloat(cliente.data.cuenta.saldo).toFixed(2) );
			} else {
				M.toast({html: data.data})
			}
			motrarIndicadorProgreso(false);
		}).fail(function (jqXHR, textStatus, errorThrown) {
			motrarIndicadorProgreso(false);
			mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
		});
	}


	//Evento de cambio en los input del formulario
	//$('#info_empresa').find('input:text').val('');
	function motrarIndicadorProgreso(status){
		if(status) {
			status_proceso = true;
			$('#indicador_progreso').addClass('active');
		} else {
			status_proceso = false;
			$('#indicador_progreso').removeClass('active');
		}
	}

	function motrarIndicadorProgresoID(id, status){
		if(status)
			$('#'+id).addClass('active');
		else
			$('#'+id).removeClass('active');
	}

	function cambiarEstadoBotonModal(id, status){
		if(status) {
			$('#'+id).addClass('noactive');
		} else {
			$('#'+id).removeClass('noactive');
		}
	}

	function mostrarMensajeDeError(jqXHR, textStatus, errorThrown){
		//Validar error y dar control de errores : status
		if (jqXHR.status === 0) {
			//alert('Not connect: Verify Network.');
			M.toast({html: 'Error, verifica tu conexión a internet'})
		} else if (jqXHR.status == 404) {
			//alert('Requested page not found [404]');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (jqXHR.status == 500) {
			//alert('Internal Server Error [500].');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'parsererror') {
			//alert('Requested JSON parse failed.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'timeout') {
			//alert('Time out error.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'abort') {
			//alert('Ajax request aborted.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else {
			//alert('Uncaught Error: ' + jqXHR.responseText);
			M.toast({html: 'No se ha podido completar la operación'})
		}
	}

	function getCliente(id){
		let cliente = null;
		lista_subclientes.forEach(function(c){
			if(id === c.id){
				cliente = c;
			}
		});
		return cliente;
	}

	function setCampoTipoA(id, val, dt){
		$('#'+id).val(val);
		$('#'+id).attr('placeholder', val);
		$('#'+id).attr('data-target', dt);
	}

	$('body').on('click', '.li_cliente', function(){
		if(!status_proceso) {
			motrarIndicadorProgreso(true);

			let cliente = getCliente($(this).attr('data-target'));
			console.table(cliente);

			if(cliente != null && !($(this).hasClass('active'))) {
				$('.li_cliente').removeClass('active');
				$(this).addClass('active');

				//limpiar campos data representante y empresa
				$('#form_data_sub_cliente').find('input:text').val('');
				$('#form_data_sub_cliente').find('select').val('none');
				$('#id_subcliente').val('');

				//Asignar Campos
				$('#id_subcliente').val(cliente.id);
				setCampoTipoA('representante', cliente.data.representante, cliente.id+'|representante|0');
				setCampoTipoA('etiqueta', cliente.data.etiqueta, cliente.id+'|etiqueta|0');
				setCampoTipoA('correo', cliente.data.correo, cliente.id+'|correo|0');
				setCampoTipoA('estado', cliente.data.estado, cliente.id+'|estado|0');
				motrarIndicadorProgreso(false);
			} else {
				motrarIndicadorProgreso(false);
			}
		} else {
			M.toast({html: 'Hay otro proceso en ejecución'});
		}
	});
	
	$('.input_usuario').blur(function(){
		let valor = $(this).val();
		if((valor.length <= 0) && ($('#id_subcliente').val().length > 0)){
			$(this).val($(this).attr('placeholder'));
		}
	});

	$('.input_usuario').keypress(function (e) {
		if(e.which ==13){
			if(!(status_proceso)) {
				if($('#id_subcliente').val().length > 0) {
					if($(this).val().length <= 0){
						if($(this).attr('data-target').length > 0){
							$(this).val($(this).attr('placeholder'));
						}
					} else {
						motrarIndicadorProgreso(true);
						//Guardar Cambio y actualizar
						let d = {
							id_campo: $(this).attr('id'),
							cliente: $(this).attr('data-target').split('|')[0],
							campo: $(this).attr('data-target').split('|')[1],
							tipo: $(this).attr('data-target').split('|')[2],
							old: $(this).attr('placeholder'),
							new: $(this).val(),
						};
						console.log(d);
						$.ajax({
							url: '/cliente/updateCampoCliente',
							type: "POST",
							cache: false,
							dataType:'json',
							data: d
						}).done(function (data) {
							M.toast({html: data.data, displayLength:1000})
							if(data.code == 0) {
								setCampoTipoA(d.id_campo, d.new, d.cliente+'|'+d.id_campo+'|0');

								lista_subclientes.forEach(c => {
									if(c.id === d.cliente){
										if(d.tipo === '0'){
											c.data[d.campo] = d.new;
										} else if(d.tipo === '1'){
											c.data[d.campo.split('.')[0]][d.campo.split('.')[1]] = d.new;
										}
									}
								});
							}
							motrarIndicadorProgreso(false);
						}).fail(function (jqXHR, textStatus, errorThrown) {
							motrarIndicadorProgreso(false);
							mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
						});
					}
				}
			} else {
				M.toast({html: 'Hay otro proceso en ejecución'});
			}
		}
	});

	$('#estado').change(function(){
		if($(this).val() !== 'none') {
			motrarIndicadorProgreso(true);
			let d = {
				cliente: $(this).attr('data-target').split('|')[0],
				campo: $(this).attr('data-target').split('|')[1],
				tipo: $(this).attr('data-target').split('|')[2],
				old: $(this).attr('placeholder'),
				new: parseInt($(this).val()),
			};

			$.ajax({
				url: '/cliente/updateCampoCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: d
			}).done(function (data) {
				M.toast({html: data.data})
				if(data.code == 0) {
					setCampoTipoA(d.id_campo, d.new, d.cliente+'|'+d.id_campo+'|0');

					lista_subclientes.forEach(c => {
						if(c.id === d.cliente){
							c.data.estado = d.new;
						}
					});
				}
				motrarIndicadorProgreso(false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgreso(false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			M.toast({html:'Error, debes seleccionar un valor valido'});
			$(this).val($(this).attr('placeholder'));
		}
	});

	$('#cuenta_licencia').change(function(){
		if($(this).val() !== 'none') {
			motrarIndicadorProgreso(true);
			let d = {
				cliente: $(this).attr('data-target').split('|')[0],
				campo: $(this).attr('data-target').split('|')[1],
				tipo: $(this).attr('data-target').split('|')[2],
				old: $(this).attr('placeholder'),
				new: $(this).val(),
			};

			$.ajax({
				url: '/admin/updateCampoCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: d
			}).done(function (data) {
				M.toast({html: data.data})
				if(data.code == 0) {
					$('#cuenta_licencia').attr('placeholder',d.new);
					$('#cuenta_licencia').val(''+d.new);

					lista_clientes.forEach(c => {
						if(c.id === d.cliente){
							c.data.cuenta.licencia.tipo = parseInt(d.new.split('/')[0]);
							c.data.cuenta.licencia.etiqueta = d.new.split('/')[1];
						}
					});
				}
				motrarIndicadorProgreso(false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgreso(false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			M.toast({html:'Error, debes seleccionar un valor valido'});
			$(this).val($(this).attr('placeholder'));
		}
	});

	function cargarHistorialDePagos(id_cliente){
		$('#lista_annos_pagos').empty();
		let lista_annos = [];
		lista_pagos.forEach(pago => {
			if(pago.data.id_cliente === id_cliente) {
				if(lista_annos.indexOf(pago.data.anno)<0) {
					lista_annos.push(pago.data.anno);
				}
			}
		});

		lista_annos.forEach(anno => {                    
			let html  = '<div class="ano">';
				html += '	<span class="anno_pagos_item" data-cliente="'+id_cliente+'" data-anno="'+anno+'">'+anno+'</span>';
				html += '</div>';
			$('#lista_annos_pagos').append(html);
		});
	}

	$('.btn_modal_action').click(function(){
		let modal = $(this).attr('data-target');
		if(modal === 'modal_historial_registro_pagos'){
			if($(this).attr('data-cliente').length > 0) {
				//Cargar historial de pagos antes de mostrar modal
				cargarHistorialDePagos($(this).attr('data-cliente'));
				$('#reg_pag_id_cliente').val($(this).attr('data-cliente'));
				$('#'+modal).addClass('active');
			}
		} else {
			$('#'+modal).addClass('active');
		}
	});

	$('.btn_close_modale').click(function(){
		let modal = $(this).attr('data-target');
		$('#'+modal).removeClass('active');
		$('#'+modal).find('input').val('');
		$('#'+modal).find('select').val('none');
	});

	function validarCamposSubCliente() {
		let status = false;
		let id_error = '';
		if(! ($('#reg_sub_id_cliente').val() !== 'none') ) {
			setError(id_error, 'Error, el cliente es requerido', 0);
		} else if(! $('#reg_sub_etiqueta').val().length > 0 ) {
			setError(id_error, 'Error, nombre de la empresa requerido', 0);
		 } else if(! $('#reg_sub_representante').val().length > 0 ) {
			setError(id_error, 'Error, nombre del representante requerido', 0);
		} else if(! $('#reg_sub_correo').val().length > 0 ) {
			setError(id_error, 'Error, correo de la empresa o representante requerido', 0);
		} else {
			status = true;
		}
		return status;
	}

	function setError(id, msg, opc){
		M.toast({html: msg, classes: 'rounded'});
	}

	$('#registrar_subcliente').click(function(){
		motrarIndicadorProgresoID('modale_progreso_subcliente', true);
		cambiarEstadoBotonModal('registrar_subcliente', true);
		if(validarCamposSubCliente()){
			let d = {
				id_cliente: $('#reg_sub_id_cliente').val(),
				etiqueta: $('#reg_sub_etiqueta').val(),
				representante: $('#reg_sub_representante').val(),
				correo: $('#reg_sub_correo').val(),
				estado: 0,
			};
			$.ajax({
				url: '/admin/registrarSubCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				setError('', data.data, 0);
				if(data.code == 0) {
					lista_subclientes.push(data.subcliente);

					let html  = '<li class="li_cliente" id="li_clientes_'+data.subcliente.id+'" data-target="'+data.subcliente.id+'">';
						html += '	<div class="vertical">';
						html += '		<i class="fas fa-briefcase"></i>';
						html += '		<span class="cliente_nombre">'+data.subcliente.data.etiqueta+'</span>';
						html += '	</div>';
						html += '</li>';
					$('#ul_clientes_lista').append(html);

					$('#modal_add_sub_cliente').find('input').val('');
				}
				motrarIndicadorProgresoID('modale_progreso_subcliente', false);
				cambiarEstadoBotonModal('registrar_subcliente', false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgresoID('modale_progreso_subcliente', false);
				cambiarEstadoBotonModal('registrar_subcliente', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			motrarIndicadorProgresoID('modale_progreso_subcliente', false);
			cambiarEstadoBotonModal('registrar_subcliente', false);
		}
	});

	function setErrorOInfo(id, msg, tipo){
		$('#'+id).text(msg);
		if(tipo === 0) {
			$('#'+id).removeClass('info');
			$('#'+id).addClass('error');
		} else  if(tipo === 1){
			$('#'+id).removeClass('error');
			$('#'+id).addClass('info');
		} else if(tipo === -1){
			$('#'+id).removeClass('error');
			$('#'+id).removeClass('info');
		}
	}

	function mostrarLoader(id, show){
		if(show) {
			$('#'+id).addClass('active');
		} else {
			$('#'+id).removeClass('active');
		}
	}

	$('#btn_modal_pagos').click(function(){
		let cliente = $(this).attr('data-target');
		if(cliente.length > 0){
			//Obtener Historial de Pagos del cliente
		}
	});

	function getDataMes(mes){
		if(mes === 1) {
			return {
				mes: mes,
				etiqueta: 'Enero'
			};
		} else if(mes === 2) {
			return {
				mes: mes,
				etiqueta: 'Febrero'
			};
		} else if(mes === 3) {
			return {
				mes: mes,
				etiqueta: 'Marzo'
			};
		} else if(mes === 4) {
			return {
				mes: mes,
				etiqueta: 'Abril'
			};
		} else if(mes === 5) {
			return {
				mes: mes,
				etiqueta: 'Mayo'
			};
		} else if(mes === 6) {
			return {
				mes: mes,
				etiqueta: 'Junio'
			};
		} else if(mes === 7) {
			return {
				mes: mes,
				etiqueta: 'Julio'
			};
		} else if(mes === 8) {
			return {
				mes: mes,
				etiqueta: 'Agosto'
			};
		} else if(mes === 9) {
			return {
				mes: mes,
				etiqueta: 'Septiembre'
			};
		} else if(mes === 10) {
			return {
				mes: mes,
				etiqueta: 'Octubre'
			};
		} else if(mes === 11) {
			return {
				mes: mes,
				etiqueta: 'Noviembre'
			};
		} else if(mes === 12) {
			return {
				mes: mes,
				etiqueta: 'Diciembre'
			};
		}
	}

	function obtenerListaMeses(cliente, anno){
		let meses = [];
		let n = [];
		
		lista_pagos.forEach(pago => {
			if(pago.data.id_cliente === cliente && pago.data.anno === anno) {
				if(n.indexOf(pago.data.mes) < 0) {
					n.push(pago.data.mes);
					meses.push(getDataMes(pago.data.mes));
				}
			}
		});

		for (let i = 0; i<meses.length; i++){
			for (let j = 0; j<meses.length; j++){
				let aux;
				if(meses[i].mes < meses[j].mes) {
					aux = meses[i];
					meses[i] = meses[j];
					meses[j] = aux;
				}
			}
		}
		
		return meses;
	}

	$('body').on('click', '.anno_pagos_item', function(){
		$('#div_pagos_por_anno').empty();
		let cliente = $(this).attr('data-cliente');
		let anno = parseInt($(this).attr('data-anno'));
		$('.anno_pagos_item').removeClass('active');
		$(this).addClass('active');
		
		let lista_meses = obtenerListaMeses(cliente, anno);
		lista_meses.forEach(mes => {
			let html  = '<li>';
				html += '	<div class="collapsible-header">';
				html += '		<i class="fas fa-calendar-alt"></i>';
				html += '		'+mes.etiqueta;
				html += '	</div>';
				html += '	<div class="collapsible-body">';
				html += '		<table>';
				html += '			<thead>';
				html += '				<tr>';
				html += '					<td>Medio</td>';
				html += '					<td>Comprobante</td>';
				html += '					<td>Fecha</td>';
				html += '					<td>Monto</td>';
				html += '				</tr>';
				html += '			</thead>';
				html += '			<tbody>';
				lista_pagos.forEach(pago => {
					if(pago.data.id_cliente === cliente && pago.data.anno === anno && pago.data.mes === mes.mes){
						html += '	<tr>';
						html += '		<td>'+pago.data.medio.etiqueta+'</td>';
						html += '		<td>'+pago.data.nro_comprobante+'</td>';
						html += '		<td>'+pago.data.dia+'-'+pago.data.mes+'-'+pago.data.anno+'</td>';
						html += '		<td>'+pago.data.monto+'</td>';
						html += '	</tr>';
					}
				});
				html += '			</tbody>';
				html += '		</table>';
				html += '	</div>';
				html += '</li>';
			
			$('#div_pagos_por_anno').append(html);
			$('#div_pagos_por_anno').collapsible();
		});
	});
});