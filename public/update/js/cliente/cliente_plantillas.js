$(document).ready(function(){
    $('.tabs').tabs();
    $('.collapsible').collapsible();

    const api_url = 'http://localhost:4000';
    //const api_url = 'https://newspoth.com';
    var  status_proceso = false;

    var config = {
        apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
        authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
        databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
        projectId: "hotspotmagazine-bf2eb",
        storageBucket: "hotspotmagazine-bf2eb.appspot.com",
        messagingSenderId: "282938551360"
    };
    firebase.initializeApp(config);
    const auth = firebase.auth();

    auth.onAuthStateChanged(function (user) {
        if (user) {
            console.log(user);
        } else {
            window.location.href = "/";
        }
    });

    //Evento de cambio en los input del formulario
    //$('#info_empresa').find('input:text').val('');

    function motrarIndicadorProgreso(status){
        if(status) {
            status_proceso = true;
            $('#indicador_progreso').addClass('active');
        } else {
            status_proceso = false;
            $('#indicador_progreso').removeClass('active');
        }
    }

    function motrarIndicadorProgresoID(id, status){
        if(status)
            $('#'+id).addClass('active');
        else
            $('#'+id).removeClass('active');
    }

    function cambiarEstadoBotonModal(id, status){
        if(status) {
            $('#'+id).addClass('noactive');
        } else {
            $('#'+id).removeClass('noactive');
        }
    }

    function mostrarMensajeDeError(jqXHR, textStatus, errorThrown){
        //Validar error y dar control de errores : status
        if (jqXHR.status === 0) {
            //alert('Not connect: Verify Network.');
            M.toast({html: 'Error, verifica tu conexión a internet'})
        } else if (jqXHR.status == 404) {
            //alert('Requested page not found [404]');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (jqXHR.status == 500) {
            //alert('Internal Server Error [500].');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'parsererror') {
            //alert('Requested JSON parse failed.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'timeout') {
            //alert('Time out error.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'abort') {
            //alert('Ajax request aborted.');
            M.toast({html: 'No se ha podido completar la operación'})
        } else {
            //alert('Uncaught Error: ' + jqXHR.responseText);
            M.toast({html: 'No se ha podido completar la operación'})
        }
    }

    //Cargar data EQUIPOS

    $('.btn_modal_action').click(function(){
        let modal = $(this).attr('data-target');
        $('#'+modal).addClass('active');
    });

    $('.btn_close_modale').click(function(){
        let modal = $(this).attr('data-target');
        $('#'+modal).removeClass('active');
        $('#'+modal).find('input').val('');
        $('#'+modal).find('select').val('none');
    });

    function setMesajeToast(msg){
        M.toast({html: msg, classes: 'rounded'});
    }

    function setErrorOInfo(id, msg, tipo){
        $('#'+id).text(msg);
        if(tipo === 0) {
            $('#'+id).removeClass('info');
            $('#'+id).addClass('error');
        } else  if(tipo === 1){
            $('#'+id).removeClass('error');
            $('#'+id).addClass('info');
        } else if(tipo === -1){
            $('#'+id).removeClass('error');
            $('#'+id).removeClass('info');
        }
    }

    function mostrarLoader(id, show){
        if(show) {
            $('#'+id).addClass('active');
        } else {
            $('#'+id).removeClass('active');
        }
    }

    /**
     * Hacia arriba 
     * Común para todos los scripts, revisar ciertos métodos para modificar funcionalidad generar
    */

    function validarCamposMaqueta() {
        let e = false;
        if(!($('#reg_maq_id_cliente').val() !== 'none')) {
            setMesajeToast('Error, debes seleccionar un cliente valido');
        } else if(! $('#reg_maq_etiqueta').val().length > 0){
            setMesajeToast('Error, debes ingresar un identificador para la maqueta');
        } else {
            e = true;
        }
        return e;
    }

    function agregarMaquetaToLista(maqueta){
        let html  = '<li class="li_item" id="li_maqueta_'+maqueta.id+'" data-target="'+maqueta.id+'">';
            html += '	<div class="vertical">';
            html += '		<i class="fas fa-briefcase"></i>';
            html += '		<span class="maqueta_etiqueta">'+maqueta.data.etiqueta+'</span>';
            html += '	</div>';
            html += '</li>';
        $('#ul_maquetas_lista').append(html);
    }

    $('#registrar_maqueta').click(function(){
        motrarIndicadorProgresoID('modale_progreso_add_maqueta', true);
        cambiarEstadoBotonModal('registrar_maqueta', true);
        if(validarCamposMaqueta()){
            let maqueta = {
                id_cliente: $('#reg_maq_id_cliente').val(),
                etiqueta: $('#reg_maq_etiqueta').val(),
                fecha: '',
                hora: '',
                artes_movil: {
                    banner: '',
                    fondo: '',
                },
                artes_pc: {
                    banner: '',
                    fondo: '',
                },
                artes_tablet: {
                    banner: '',
                    fondo: '',
                },
                iconos: {
                    a: {
                        etiqueta: '',
                        icono: '',
                        tipo: '',
                        titulo: '',
                    },
                    b: {
                        etiqueta: '',
                        icono: '',
                        tipo: '',
                        titulo: '',
                    },
                    c: {
                        etiqueta: '',
                        icono: '',
                        tipo: '',
                        titulo: '',
                    },
                    d: {
                        etiqueta: '',
                        icono: '',
                        tipo: '',
                        titulo: '',
                    }
                },
                status_cajas: {
                    facebook: 0,
                    login_by_mac: 0,
                    login_by_trial: 0
                }
            };
            //
            $.ajax({
                url: '/cliente/registrarMaqueta',
                type: "POST",
                cache: false,
                dataType:'json',
                data: {
                    data: JSON.stringify(maqueta)
                }
            }).done(function (data) {
                M.toast({html: data.data})
                if(data.code == 0) {
                    lista_maquetas.push(data.maqueta);
                    agregarMaquetaToLista(data.maqueta)
                    $('#modal_add_maqueta').find('input').val('');
                    $('#modal_add_maqueta').find('select').val('none');
                }
                motrarIndicadorProgresoID('modale_progreso_add_maqueta', false);
                cambiarEstadoBotonModal('registrar_maqueta', false);
            }).fail(function (jqXHR, textStatus, errorThrown) {
                mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
            });
        }else{
            motrarIndicadorProgresoID('modale_progreso_add_maqueta', false);
            cambiarEstadoBotonModal('registrar_maqueta', false);
        }
    });

    function getMaqueta(id_maqueta) {
        let maqueta = null;
        lista_maquetas.forEach(m => {
            if(m.id === id_maqueta) {
                maqueta = m;
            }
        });
        return maqueta;
    }

    function setValorCampoTipoA(id, val, dt) {
        $('#'+id).val(val);
        $('#'+id).attr('placeholder', val);
        $('#'+id).attr('data-target', dt);
    }

    function setValorCampoTipoB(id, val, dt) {
        $('#'+id).prop('checked', (val === 1)? true : false);
        $('#'+id).attr('placeholder', val);
        $('#'+id).attr('data-target', dt);
    }

    function setValorCampoTipoC(preview, input, val, dt) {
        console.log(preview);
        console.log(val);
        if(preview.indexOf('icono_') >= 0) {
            $('#'+preview).css({
                'background-image': 'url('+val+')',
                'background-size':'100% 100%',
                'background-repeat':'no-repeat',
                'background-position':'center',
            });
        } else {
            $('#'+preview).css({
                'background-image': 'url()',
                'background-size':'100% 90%',
                'background-repeat':'no-repeat',
                'background-position':'center',
            });
            $('#'+preview).css({
                'background-image': 'url('+val+')',
                'background-size':'100% 90%',
                'background-repeat':'no-repeat',
                'background-position':'center',
            });
        }
        $('#'+input).attr('placeholder', val);
        $('#'+input).attr('data-target', dt);
    }

    function setAttrCampoTipoA(id, a, b){
        $('#'+id).attr({
            'data-tipo': a,
            'data-cliente': b,
        });
    }

    $('body').on('click', '.li_item', function(){
        if(!status_proceso) {
            motrarIndicadorProgreso(true);
            
            let id_maqueta = $(this).attr('data-target');
            let maqueta = getMaqueta(id_maqueta);

			if(maqueta != null && !($(this).hasClass('active'))) {
                $('.li_item').removeClass('active');
                $(this).addClass('active');

                //Limpiar Campos
                $('#row_form_maqueta').find('input:text, input:hidden').val('');
                $('#row_form_maqueta').find('input:text').attr('placeholder', '');
                $('#row_form_maqueta').find('input:checkbox').prop('checked', false);
                $('#row_form_maqueta').find('input:file').attr('placeholder', '');
                $('#row_form_maqueta').find('input:file').attr('data-target', '');
                $('.div_with_fondo').css('background-image', 'none');
                $('.icono_button_modal').removeAttr('data-tipo');
                $('.icono_button_modal').removeAttr('data-cliente');

                $('#id_maqueta').val(maqueta.id);
                
                let { id_cliente, etiqueta, fecha, hora , status_cajas, artes_pc, artes_tablet, artes_movil, iconos, url_fb_like } = maqueta.data;
                setValorCampoTipoA('id_cliente', id_cliente, id_maqueta+'|id_cliente|0');
                setValorCampoTipoA('etiqueta', etiqueta, id_maqueta+'|etiqueta|0');
                setValorCampoTipoA('fecha_hora', fecha+', '+hora, id_maqueta+'|fecha_hora|0');
                
                setValorCampoTipoB('status_facebook', status_cajas.facebook, id_maqueta+'|status_cajas.facebook|1');
                setValorCampoTipoB('status_login_by_mac', status_cajas.login_by_mac, id_maqueta+'|status_cajas.login_by_mac|1');
                setValorCampoTipoB('status_login_by_trial', status_cajas.login_by_trial, id_maqueta+'|status_cajas.login_by_trial|1');
                setValorCampoTipoA('url_fb_like', url_fb_like, id_maqueta+'|url_fb_like|0');

                setValorCampoTipoC('banner_pc_preview', 'banner_pc_input', artes_pc.banner, id_maqueta+'|artes_pc.banner|1|banner_pc');
                setValorCampoTipoC('fondo_pc_preview', 'fondo_pc_input', artes_pc.fondo, id_maqueta+'|artes_pc.fondo|1|fondo_pc');
                
                setValorCampoTipoC('banner_tab_preview', 'banner_tab_input', artes_tablet.banner, id_maqueta+'|artes_tablet.banner|1|banner_tab');
                setValorCampoTipoC('fondo_tab_preview', 'fondo_tab_input', artes_tablet.fondo, id_maqueta+'|artes_tablet.fondo|1|fondo_tab');

                setValorCampoTipoC('banner_mov_preview', 'banner_mov_input', artes_movil.banner, id_maqueta+'|artes_movil.banner|1|banner_mov');
                setValorCampoTipoC('fondo_mov_preview', 'fondo_mov_input', artes_movil.fondo, id_maqueta+'|artes_movil.fondo|1|fondo_mov');

                setValorCampoTipoC('icono_a_preview', 'icono_a_input', iconos.a.icono, id_maqueta+'|iconos.a.icono|2|icono_a');
                setValorCampoTipoC('icono_b_preview', 'icono_b_input', iconos.b.icono, id_maqueta+'|iconos.b.icono|2|icono_b');
                setValorCampoTipoC('icono_c_preview', 'icono_c_input', iconos.c.icono, id_maqueta+'|iconos.c.icono|2|icono_c');
                setValorCampoTipoC('icono_d_preview', 'icono_d_input', iconos.d.icono, id_maqueta+'|iconos.d.icono|2|icono_d');

                setValorCampoTipoA('icono_a_etiqueta', iconos.a.etiqueta, id_maqueta+'|iconos.a.etiqueta|2|icono_a');
                setValorCampoTipoA('icono_b_etiqueta', iconos.b.etiqueta, id_maqueta+'|iconos.b.etiqueta|2|icono_a');
                setValorCampoTipoA('icono_c_etiqueta', iconos.c.etiqueta, id_maqueta+'|iconos.c.etiqueta|2|icono_a');
                setValorCampoTipoA('icono_d_etiqueta', iconos.d.etiqueta, id_maqueta+'|iconos.d.etiqueta|2|icono_a');
            
                setValorCampoTipoA('icono_a_tipo', iconos.a.tipo, id_maqueta+'|iconos.a.tipo|2|icono_a');
                setValorCampoTipoA('icono_b_tipo', iconos.b.tipo, id_maqueta+'|iconos.b.tipo|2|icono_a');
                setValorCampoTipoA('icono_c_tipo', iconos.c.tipo, id_maqueta+'|iconos.c.tipo|2|icono_a');
                setValorCampoTipoA('icono_d_tipo', iconos.d.tipo, id_maqueta+'|iconos.d.tipo|2|icono_a');

                setValorCampoTipoA('icono_a_titulo', iconos.a.titulo, id_maqueta+'|iconos.a.titulo|2|icono_a');
                setValorCampoTipoA('icono_b_titulo', iconos.b.titulo, id_maqueta+'|iconos.b.titulo|2|icono_a');
                setValorCampoTipoA('icono_c_titulo', iconos.c.titulo, id_maqueta+'|iconos.c.titulo|2|icono_a');
                setValorCampoTipoA('icono_d_titulo', iconos.d.titulo, id_maqueta+'|iconos.d.titulo|2|icono_a');

                setAttrCampoTipoA('icono_a_cargar_contenido', iconos.a.tipo, id_cliente);
                setAttrCampoTipoA('icono_b_cargar_contenido', iconos.b.tipo, id_cliente);
                setAttrCampoTipoA('icono_c_cargar_contenido', iconos.c.tipo, id_cliente);
                setAttrCampoTipoA('icono_d_cargar_contenido', iconos.d.tipo, id_cliente);
                
                motrarIndicadorProgreso(false);
            }
        } else {
			M.toast({html: 'Hay otro proceso en ejecución'});
		}
    });

    $('.btn_file').click(function(){
        if($('#id_maqueta').val().length > 0) {
            $('#'+$(this).attr('data-input')).click();
        }
    });

    function peticionAjaxServidor(d, type, dataType, url, cb) {
        $.ajax({
            url: url,
            type: type,
            cache: false,
            dataType: dataType,
            data: {
                data: JSON.stringify(d)
            }
        }).done(function (d) {
            cb(d);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
            cb({code: -2, data: ''});
        });
    }

    function updateCampoMaqueta(d){
        lista_maquetas.forEach(m =>  {
            if(m.id === d.id_maqueta) {
                if(d.tipo === 1){
                    m.data[d.campo.split('.')[0]][d.campo.split('.')[1]] = d.valor;
                } else if(d.tipo === 2) {
                    m.data[d.campo.split('.')[0]][d.campo.split('.')[1]][d.campo.split('.')[2]] = d.valor;
                }
            }
        });
    }

    $('.check_status').change(function(){
        if($('#id_maqueta').val().length > 0) {
            if(!status_proceso) {
                motrarIndicadorProgreso(true);
                let d = {
                    id_maqueta: $(this).attr('data-target').split('|')[0],
                    campo: $(this).attr('data-target').split('|')[1],
                    tipo: parseInt($(this).attr('data-target').split('|')[2]),
                    old: parseInt($(this).attr('placeholder')),
                    valor: ($(this).prop('checked')) ? 1 : 0
                };
                peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoMaqueta', (data) => {
                    if(data.code !== -2)
                        M.toast({html: data.data});
                    
                    if(data.code === 0) { 
                        updateCampoMaqueta(d);
                    }
                    motrarIndicadorProgreso(false);
                });
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
                $(this).prop('checked', ($(this).prop('checked')?false:true));    
            }
        } else {
            $(this).prop('checked', false);
        }
    });

    function subirArchivoServidor(input, tipo, id_maqueta, cb){
        var formData = new FormData(); 
        //Get fotos  file, tipo, id_plantilla
        formData.append('file', input.files[0]);
        formData.append('tipo', tipo);
        formData.append('id_maqueta', id_maqueta);

        $.ajax({
            url: user.data.urls.api_node + '/spot/uploadFileMaqueta',
            type:"POST",
            data: formData,
            contentType: false,
            processData: false,
        }).done(function (data) {
            console.log(data);
            cb(data);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            cb({code:-1, data: 'Error al subir el archivo'});
        });
    }

    function subirArchivoServidorConRuta($input, url, path, cb){
        var formData = new FormData(); 
        formData.append('file', $input[0].files[0]);
        formData.append('path', path);
        $.ajax({
            url: user.data.urls.api_node + url,
            type:"POST",
            data: formData,
            contentType: false,
            processData: false,
        }).done(function (data) {
            cb(data);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            console.log(errorThrown);
            cb({code:-1, data: 'Error al subir el archivo'});
        });
    }

    $('.file_maqueta').change(function(){
        if($('#id_maqueta').val().length > 0) {
            if(!status_proceso) {
                motrarIndicadorProgreso(true);
                let input = $(this);
                subirArchivoServidor(this, input.attr('data-tipo'), $('#id_maqueta').val(), (data) => {
                    if(data.code === 0) {
                        let d = {
                            id_maqueta: input.attr('data-target').split('|')[0],
                            campo: input.attr('data-target').split('|')[1],
                            tipo: parseInt(input.attr('data-target').split('|')[2]),
                            name: input.attr('data-target').split('|')[3],
                            old: input.attr('placeholder'),
                            valor: data.url
                        };
                        peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoMaqueta', (data_b) => {
                            if(data_b.code !== -2)
                                M.toast({html: data_b.data});
                            
                            if(data_b.code === 0) {
                                updateCampoMaqueta(d);
                                //UpdatePreviewFile 
                                setValorCampoTipoC(d.name+'_preview', input.attr('id'), d.valor, d.id_maqueta+'|'+d.campo+'|'+d.tipo+'|'+d.name);
                            }
                            motrarIndicadorProgreso(false);
                        });
                    } else {
                        motrarIndicadorProgreso(false);
                        M.toast({html: data.data});
                    }
                });
                
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });


    $('.input_maqueta').keypress(function (e) {
        if(e.which ==13){
            if($('#id_maqueta').val().length > 0) {
                if(!status_proceso) {
                    let input = $(this);
                    if( ((input.val().length > 0 && input.val().length <= 8) || (input.val().length > 0 && input.val().length <= 28) || (input.val().length > 0 && input.val().length <= 160)) && (input.val() !== input.attr('placeholder'))){
                        motrarIndicadorProgreso(true);
                        let d = {
                            id_maqueta: input.attr('data-target').split('|')[0],
                            campo: input.attr('data-target').split('|')[1],
                            tipo: parseInt(input.attr('data-target').split('|')[2]),
                            name: input.attr('data-target').split('|')[3],
                            old: input.attr('placeholder'),
                            valor: input.val()
                        };
                        peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoMaqueta', (data_b) => {
                            if(data_b.code !== -2)
                                M.toast({html: data_b.data});
                            
                            if(data_b.code === 0) {
                                updateCampoMaqueta(d);                                
                                setValorCampoTipoA(input.attr('id'), d.valor, d.id_maqueta+'|'+d.campo+'|'+d.tipo+'|'+d.name);
                            }
                            motrarIndicadorProgreso(false);
                        });
                    } else {
                        input.val(input.attr('placeholder'));
                    }
                }else{
                    M.toast({html: 'Existe otro proceso en ejecución'});
                }
            }
        }
    });

    $('.select_maqueta').change(function(){
        if($('#id_maqueta').val().length > 0) {
            if(!status_proceso) {
                let input = $(this);
                if( (input.val() !== 'none') && (input.val() !== input.attr('placeholder'))) {
                    motrarIndicadorProgreso(true);
                    let d = {
                        id_maqueta: input.attr('data-target').split('|')[0],
                        campo: input.attr('data-target').split('|')[1],
                        tipo: parseInt(input.attr('data-target').split('|')[2]),
                        name: input.attr('data-target').split('|')[3],
                        old: input.attr('placeholder'),
                        valor: input.val()
                    };
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoMaqueta', (data_b) => {
                        if(data_b.code !== -2)
                            M.toast({html: data_b.data});

                        if(data_b.code === 0) {
                            updateCampoMaqueta(d);
                            setAttrCampoTipoA(d.name+'_cargar_contenido', d.valor, $('#id_cliente').val());
                            setValorCampoTipoA(input.attr('id'), d.valor, d.id_maqueta+'|'+d.campo+'|'+d.tipo+'|'+d.name);
                        }
                        motrarIndicadorProgreso(false);
                    });
                } else {
                    input.val(input.attr('placeholder'));
                }
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });

    function setContenidoPorModulo(tipo, modulo){
        switch(tipo){
            case 0:
                //Video
                $('#lista_a_contenido_0').empty();
                if(typeof(modulo.data.contenido) !== 'undefined') {
                    modulo.data.contenido.b.forEach(item => {
                        let html  = '<li id="li_video_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-file-video"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_video" data-target="'+item.id+'" data-nivel="1">Eliminar</a>';
                            html += '       <video controls muted>';
                            html += '           <source src="'+item.url+'">';
                            html += '       </video>';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_0').append(html);
                        $('#lista_a_contenido_0').collapsible();
                    });
                }
                break;
            case 1:
                //Promociones
                $('#lista_a_contenido_1').empty();
                if(typeof(modulo.data.contenido) !== 'undefined') {
                    modulo.data.contenido.b.forEach(item => {
                        let html  = '<li id="li_promo_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-file-image"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_promo" data-target="'+item.id+'" data-nivel="1">Eliminar</a>';
                            html += '       <img src="'+item.url+'">';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';       
                        $('#lista_a_contenido_1').append(html);
                        $('#lista_a_contenido_1').collapsible();
                    });
                }
                break;
            case 2:
                //Turismo
                $('#lista_a_contenido_2').empty();
                if(typeof(modulo.data.contenido) !== 'undefined') {
                    modulo.data.contenido.b.forEach(item => {
                        let html  = '<li id="li_lugar_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-map-marked"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_turimos" data-target="'+item.id+'" data-nivel="1">Eliminar</a>';
                            html += '       <img src="'+item.url+'">';
                            html += '       <p><b>Categoría: </b> '+item.categoria.split('/')[1]+'</p>';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_2').append(html);
                        $('#lista_a_contenido_2').collapsible();
                    });
                }
                break;
            case 3:
                //Agenda
                $('#lista_a_contenido_3').empty();
                if(typeof(modulo.data.contenido) !== 'undefined') {
                    modulo.data.contenido.b.forEach(item => {
                        let html  = '<li id="li_evento_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-calendar"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_evento" data-target="'+item.id+'" data-nivel="1">Eliminar</a>';
                            html += '       <p><b>Fecha: </b> '+item.fecha+'</p>';
                            html += '       <p><b>Hora: </b> '+item.hora+'</p>';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_3').append(html);
                        $('#lista_a_contenido_3').collapsible();
                    });
                }
                break;
            case 4:
                //Puntos
                break;
            case 5:
                //Visitas
                $('#lista_a_contenido_5').empty();
                if(typeof(modulo.data.contenido) !== 'undefined') {
                    modulo.data.contenido.b.forEach(item => {
                        let html  = '<li id="li_visita_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-calendar"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_visitas" data-target="'+item.id+'" data-nivel="1">Eliminar</a>';
                            html += '       <p><b>Latitud: </b> '+item.lat+'</p>';
                            html += '       <p><b>Logintud: </b> '+item.lng+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_5').append(html);
                        $('#lista_a_contenido_5').collapsible();
                    });
                }
                break;
        }
    }

    $('.file_video_modulo').change(function(){
        let input = this;
        if (input.files && input.files[0]) {           
            //show preview
            let $source = $('#'+$(this).attr('data-file')+'_preview_source');
            $source[0].src = URL.createObjectURL(this.files[0]);
            $source.parent()[0].load();
        }
    });

    $('.file_imagen_modulo').change(function(){
        let inp = $(this);
        let input = this;
        if (input.files && input.files[0]) {           
            var reader = new FileReader();
            reader.readAsDataURL(input.files[0]);
            reader.onload = function (e) {
                //Poner la imagen en preview
                let id = '#'+inp.attr('data-file')+'_preview';
                $(id).attr('src', e.target.result);
            }
        }
    });
    
    $('.icono_button_modal').click(function(){
        if($('#id_maqueta').val().length > 0) {
            if(!status_proceso) {
                motrarIndicadorProgreso(true);
                let input = $(this);
                let d = {
                    id_cliente: input.attr('data-cliente'),
                    button: input.attr('data-button'),
                    tipo: input.attr('data-tipo'),
                };
                if(d.tipo !== '4/Puntos Wifi') {
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/getDataModuloMaqueta', function(data){
                        if(data.code === 0) { 
                            $('#setting_id_modulo').val(data.modulo.id);
                            $('#setting_id_cliente').val(d.id_cliente);
                            $('#setting_tipo').val(d.tipo);
                            
                            $('.modulo_contenido').addClass('hide');
                            $('#contenido_'+d.tipo.split('/')[0]).removeClass('hide');

                            //Setear contenido segun modulo
                            setContenidoPorModulo(parseInt(d.tipo.split('/')[0]), data.modulo);

                            $('#modal_setting_botones').addClass('active');
                        } else  if(data.code !== -2){
                            M.toast({html: data.data});
                        }
                        motrarIndicadorProgreso(false);
                    });    
                } else {
                    motrarIndicadorProgreso(false);
                    M.toast({html: 'La información de los puntos wifi se configura de forma automatica en base a tus hotspots'});
                }
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });

    /* Videos */
    $('#reg_item_cnt0_button').click(function(){
        let d = {
            id_maqueta: $('#id_maqueta').val(),
            name: '',
            id_modulo: $('#setting_id_modulo').val(),
            id_cliente: $('#setting_id_cliente').val(),
            tipo: parseInt($('#setting_tipo').val().split('/')[0]),
            nivel: 1,
            item: {
                etiqueta: $('#reg_item_cnt0_etiqueta').val(),
                url: '',
                descripcion: $('#reg_item_cnt0_descripcion').val(),
            }
        };

        if($('#reg_item_cnt0_etiqueta').val().length > 0  && $('#reg_item_cnt0_descripcion').val().length > 0  && $('#reg_item_cnt0_url_input')[0].files && $('#reg_item_cnt0_url_input')[0].files[0]) {
            cambiarEstadoBotonModal('reg_item_cnt0_button', true);
            motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
            d.name = $('#reg_item_cnt0_url_input')[0].files[0].name;
            subirArchivoServidorConRuta($('#reg_item_cnt0_url_input'), '/spot/uploadFileMaquetaModuloContenido', 'modulos/video/video_name_'+d.name, data => {
                if(data.code === 0) {
                    d.item.url = data.url;
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/agregarContenidoModulo', (data_b) => {
                        //Agregar contenido del modulo a la lista
                        let { item } = data_b;
                        let html  = '<li id="li_video_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-file-video"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_video" data-target="'+item.id+'" data-nivel="'+d.nivel+'">Eliminar</a>';
                            html += '       <video controls muted>';
                            html += '           <source src="'+item.url+'">';
                            html += '       </video>';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_0').append(html);
                        $('#lista_a_contenido_0').collapsible();

                        //Limpiar campos
                        $('#reg_item_cnt0_etiqueta').val('');
                        $('#reg_item_cnt0_descripcion').val('');
                        $('#reg_item_cnt0_url_preview_source').attr('src', '');
                        $('#reg_item_cnt0_url_preview_source').parent()[0].load();
                        cambiarEstadoBotonModal('reg_item_cnt0_button', false);
                        motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    });
                } else  if(code !== -2){
                    cambiarEstadoBotonModal('reg_item_cnt0_button', false);
                    motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    M.toast({html: 'Error, no se pudo completar la operación'});
                }
            });
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('body').on('click', '.eliminar_item_video', function(){
        motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
        let d = {
            id_modulo: $('#setting_id_modulo').val(),
            id_item: parseInt($(this).attr('data-target')),
            nivel: parseInt($(this).attr('data-nivel'))
        };

        peticionAjaxServidor(d, 'POST', 'json', '/admin/removeContenidoModulo', (data_b) => {
            if(data_b.code === 0) {
                $('#li_video_'+d.id_item).remove();
                $('#lista_a_contenido_0').collapsible();
                M.toast({html:data_b.data});
            } else  if(data_b.code !== -2){
                M.toast({html: 'Error, no se pudo completar la operación'});
            }
            motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
        });

    });

    /* Promociones */
    $('#reg_item_cnt1_button').click(function(){
        let d = {
            id_maqueta: $('#id_maqueta').val(),
            name: '',
            id_modulo: $('#setting_id_modulo').val(),
            id_cliente: $('#setting_id_cliente').val(),
            tipo: parseInt($('#setting_tipo').val().split('/')[0]),
            nivel: 1,
            item: {
                etiqueta: $('#reg_item_cnt1_etiqueta').val(),
                url: '',
                descripcion: $('#reg_item_cnt1_descripcion').val(),
            }
        };

        if($('#reg_item_cnt1_etiqueta').val().length > 0  && $('#reg_item_cnt1_descripcion').val().length > 0  && $('#reg_item_cnt1_url_input')[0].files && $('#reg_item_cnt1_url_input')[0].files[0]) {
            
            cambiarEstadoBotonModal('reg_item_cnt1_button', true);
            motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
            
            d.name = $('#reg_item_cnt1_url_input')[0].files[0].name;
            subirArchivoServidorConRuta($('#reg_item_cnt1_url_input'), '/spot/uploadFileMaquetaModuloContenido', 'modulos/promociones/promo_name_'+d.name, data => {
                console.log(data);
                if(data.code === 0) {
                    d.item.url = data.url;
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/agregarContenidoModulo', (data_b) => {
                        //Agregar contenido del modulo a la lista
                        let { item } = data_b;
                        let html  = '<li id="li_promo_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-file-image"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_promo" data-target="'+item.id+'" data-nivel="'+d.nivel+'">Eliminar</a>';
                            html += '       <img src="'+item.url+'">';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_1').append(html);
                        $('#lista_a_contenido_1').collapsible();

                        //Limpiar campos
                        $('#reg_item_cnt1_etiqueta').val('');
                        $('#reg_item_cnt1_descripcion').val('');
                        $('#reg_item_cnt1_url_preview').attr('src', '');

                        cambiarEstadoBotonModal('reg_item_cnt1_button', false);
                        motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    });
                } else  if(data.code !== -2){
                    cambiarEstadoBotonModal('reg_item_cnt1_button', false);
                    motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    M.toast({html: 'Error, no se pudo completar la operación'});
                }
            });
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('body').on('click', '.eliminar_item_promo', function(){
        motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
        let d = {
            id_modulo: $('#setting_id_modulo').val(),
            id_item: parseInt($(this).attr('data-target')),
            nivel: parseInt($(this).attr('data-nivel'))
        };

        peticionAjaxServidor(d, 'POST', 'json', '/admin/removeContenidoModulo', (data_b) => {
            if(data_b.code === 0) {
                $('#li_promo_'+d.id_item).remove();
                $('#lista_a_contenido_1').collapsible();
                M.toast({html:data_b.data});
            } else  if(data_b.code !== -2){
                M.toast({html: 'Error, no se pudo completar la operación'});
            }
            motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
        });

    });

    /* Turísmo */

    $('#reg_item_cnt2_button').click(function(){
        let d = {
            id_maqueta: $('#id_maqueta').val(),
            name: '',
            id_modulo: $('#setting_id_modulo').val(),
            id_cliente: $('#setting_id_cliente').val(),
            tipo: parseInt($('#setting_tipo').val().split('/')[0]),
            nivel: 1,
            item: {
                etiqueta: $('#reg_item_cnt2_etiqueta').val(),
                categoria: $('#reg_item_cnt2_categoria').val(),
                url: '',
                descripcion: $('#reg_item_cnt2_descripcion').val(),
            }
        };

        if($('#reg_item_cnt2_etiqueta').val().length > 0  && $('#reg_item_cnt2_descripcion').val().length > 0  && $('#reg_item_cnt2_categoria').val() !== 'none' && $('#reg_item_cnt2_url_input')[0].files && $('#reg_item_cnt2_url_input')[0].files[0]) {
            
            cambiarEstadoBotonModal('reg_item_cnt2_button', true);
            motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
            
            d.name = $('#reg_item_cnt2_url_input')[0].files[0].name;
            subirArchivoServidorConRuta($('#reg_item_cnt2_url_input'), '/spot/uploadFileMaquetaModuloContenido', 'modulos/turismo/lugar_name_'+d.name, data => {
                if(data.code === 0) {
                    d.item.url = data.url;
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/agregarContenidoModulo', (data_b) => {
                        //Agregar contenido del modulo a la lista
                        let { item } = data_b;
                        let html  = '<li id="li_lugar_'+item.id+'" data-id="'+item.id+'">';
                            html += '   <div class="collapsible-header">';
                            html += '       <i class="fas fa-map-marked"></i>';
                            html += '       '+item.etiqueta;
                            html += '   </div>';
                            html += '   <div class="collapsible-body">';
                            html += '       <a class="eliminar_item_turimos" data-target="'+item.id+'" data-nivel="'+d.nivel+'">Eliminar</a>';
                            html += '       <img src="'+item.url+'">';
                            html += '       <p><b>Categoría: </b> '+item.categoria.split('/')[1]+'</p>';
                            html += '       <p>'+item.descripcion+'</p>';
                            html += '   </div>';
                            html += '</li>';
                        $('#lista_a_contenido_2').append(html);
                        $('#lista_a_contenido_2').collapsible();

                        //Limpiar campos
                        $('#reg_item_cnt2_etiqueta').val('');
                        $('#reg_item_cnt2_categoria').val('none');
                        $('#reg_item_cnt2_descripcion').val('');
                        $('#reg_item_cnt2_url_preview').attr('src', '');

                        cambiarEstadoBotonModal('reg_item_cnt2_button', false);
                        motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    });
                } else  if(code !== -2){
                    cambiarEstadoBotonModal('reg_item_cnt2_button', false);
                    motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
                    M.toast({html: 'Error, no se pudo completar la operación'});
                }
            });
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('body').on('click', '.eliminar_item_turimos', function(){
        motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
        let d = {
            id_modulo: $('#setting_id_modulo').val(),
            id_item: parseInt($(this).attr('data-target')),
            nivel: parseInt($(this).attr('data-nivel'))
        };

        peticionAjaxServidor(d, 'POST', 'json', '/admin/removeContenidoModulo', (data_b) => {
            if(data_b.code === 0) {
                $('#li_lugar_'+d.id_item).remove();
                $('#lista_a_contenido_2').collapsible();
                M.toast({html:data_b.data});
            } else  if(data_b.code !== -2){
                M.toast({html: 'Error, no se pudo completar la operación'});
            }
            motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
        });

    });

    /* Agenda */

    $('#reg_item_cnt3_button').click(function(){
        let d = {
            id_maqueta: $('#id_maqueta').val(),
            name: '',
            id_modulo: $('#setting_id_modulo').val(),
            id_cliente: $('#setting_id_cliente').val(),
            tipo: parseInt($('#setting_tipo').val().split('/')[0]),
            nivel: 1,
            item: {
                etiqueta: $('#reg_item_cnt3_etiqueta').val(),
                fecha: $('#reg_item_cnt3_fecha').val(),
                hora: $('#reg_item_cnt3_hora').val(),
                descripcion: $('#reg_item_cnt3_descripcion').val(),
            }
        };

        if($('#reg_item_cnt3_etiqueta').val().length > 0  && $('#reg_item_cnt3_fecha').val().length > 0  && $('#reg_item_cnt3_hora').val().length > 0 && $('#reg_item_cnt3_descripcion').val().length > 0) {
            
            cambiarEstadoBotonModal('reg_item_cnt3_button', true);
            motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
            
            peticionAjaxServidor(d, 'POST', 'json', '/admin/agregarContenidoModulo', (data_b) => {
                //Agregar contenido del modulo a la lista
                let { item } = data_b;
                let html  = '<li id="li_evento_'+item.id+'" data-id="'+item.id+'">';
                    html += '   <div class="collapsible-header">';
                    html += '       <i class="fas fa-calendar"></i>';
                    html += '       '+item.etiqueta;
                    html += '   </div>';
                    html += '   <div class="collapsible-body">';
                    html += '       <a class="eliminar_item_evento" data-target="'+item.id+'" data-nivel="'+d.nivel+'">Eliminar</a>';
                    html += '       <p><b>Fecha: </b> '+item.fecha+'</p>';
                    html += '       <p><b>Hora: </b> '+item.hora+'</p>';
                    html += '       <p>'+item.descripcion+'</p>';
                    html += '   </div>';
                    html += '</li>';
                $('#lista_a_contenido_3').append(html);
                $('#lista_a_contenido_3').collapsible();

                //Limpiar campos
                $('#reg_item_cnt3_etiqueta').val('');
                $('#reg_item_cnt3_fecha').val('');
                $('#reg_item_cnt3_hora').val('');
                $('#reg_item_cnt3_descripcion').val('');

                cambiarEstadoBotonModal('reg_item_cnt3_button', false);
                motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
            });
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('body').on('click', '.eliminar_item_evento', function(){
        motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
        let d = {
            id_modulo: $('#setting_id_modulo').val(),
            id_item: parseInt($(this).attr('data-target')),
            nivel: parseInt($(this).attr('data-nivel'))
        };

        peticionAjaxServidor(d, 'POST', 'json', '/admin/removeContenidoModulo', (data_b) => {
            if(data_b.code === 0) {
                $('#li_evento_'+d.id_item).remove();
                $('#lista_a_contenido_3').collapsible();
                M.toast({html:data_b.data});
            } else  if(data_b.code !== -2){
                M.toast({html: 'Error, no se pudo completar la operación'});
            }
            motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
        });

    });

    /* Vositas 360º */

    $('#reg_item_cnt5_button').click(function(){
        let d = {
            id_maqueta: $('#id_maqueta').val(),
            name: '',
            id_modulo: $('#setting_id_modulo').val(),
            id_cliente: $('#setting_id_cliente').val(),
            tipo: parseInt($('#setting_tipo').val().split('/')[0]),
            nivel: 1,
            item: {
                etiqueta: $('#reg_item_cnt5_etiqueta').val(),
                lat: $('#reg_item_cnt5_lat').val(),
                lng: $('#reg_item_cnt5_lng').val(),
            }
        };

        if($('#reg_item_cnt5_etiqueta').val().length > 0  && $('#reg_item_cnt5_lat').val().length > 0  && $('#reg_item_cnt5_lng').val().length > 0 ) {
            
            cambiarEstadoBotonModal('reg_item_cnt5_button', true);
            motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
            
            peticionAjaxServidor(d, 'POST', 'json', '/admin/agregarContenidoModulo', (data_b) => {
                //Agregar contenido del modulo a la lista
                let { item } = data_b;
                let html  = '<li id="li_visita_'+item.id+'" data-id="'+item.id+'">';
                    html += '   <div class="collapsible-header">';
                    html += '       <i class="fas fa-calendar"></i>';
                    html += '       '+item.etiqueta;
                    html += '   </div>';
                    html += '   <div class="collapsible-body">';
                    html += '       <a class="eliminar_item_visitas" data-target="'+item.id+'" data-nivel="'+d.nivel+'">Eliminar</a>';
                    html += '       <p><b>Latitud: </b> '+item.lat+'</p>';
                    html += '       <p><b>Logintud: </b> '+item.lng+'</p>';
                    html += '   </div>';
                    html += '</li>';
                $('#lista_a_contenido_5').append(html);
                $('#lista_a_contenido_5').collapsible();

                //Limpiar campos
                $('#reg_item_cnt5_etiqueta').val('');
                $('#reg_item_cnt5_lat').val('');
                $('#reg_item_cnt5_lng').val('');

                cambiarEstadoBotonModal('reg_item_cnt5_button', false);
                motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
            });
        } else {
            M.toast({html: 'Error, existen campos incompletos'});
        }
    });

    $('body').on('click', '.eliminar_item_visitas', function(){
        motrarIndicadorProgresoID('modale_progreso_setting_botones', true);
        let d = {
            id_modulo: $('#setting_id_modulo').val(),
            id_item: parseInt($(this).attr('data-target')),
            nivel: parseInt($(this).attr('data-nivel'))
        };

        peticionAjaxServidor(d, 'POST', 'json', '/admin/removeContenidoModulo', (data_b) => {
            if(data_b.code === 0) {
                $('#li_visita_'+d.id_item).remove();
                $('#lista_a_contenido_3').collapsible();
                M.toast({html:data_b.data});
            } else  if(data_b.code !== -2){
                M.toast({html: 'Error, no se pudo completar la operación'});
            }
            motrarIndicadorProgresoID('modale_progreso_setting_botones', false);
        });

    });

});