$(document).ready(function(){
    
    var config = {
        apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
        authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
        databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
        projectId: "hotspotmagazine-bf2eb",
        storageBucket: "hotspotmagazine-bf2eb.appspot.com",
        messagingSenderId: "282938551360"
    };
    firebase.initializeApp(config);
    const auth = firebase.auth();
    const db = firebase.firestore();
    db.settings({
        timestampsInSnapshots: true
    });

    auth.onAuthStateChanged(function (user) {
        if (user) {
            db.collection('clientes').doc(user.uid).get()
                .then(user => {
                    console.log(user.data());
                    if(user) {
                        if(user.data().tipo === 0) {
                            window.location.href = '/admin/clientes?a='+user.id;
                        } else {
                            window.location.href = '/cliente/clientes?a='+user.id;
                        }
                    }
                }).catch(error => {
                    if(!$('#loader').hasClass('hide'))
                        $('#loader').addClass('hide');
                    $('#btn_login').removeClass('noactive');
                    M.toast({html: 'Error, no se pudo completar la operación'});
                });
        } else {
            //window.location.href = "/";
        }
    });

    var show = false;

    $('#cerrar_btn').click(function(){
        if(show) {
            $('#bloque').removeClass('mini');
            $('#formulario').removeClass('show');
            
            show = false;
        }
    }); 

    $('#login_show_btn').click(function(){
        if(show) {
            $('#bloque').removeClass('mini');
            $('#formulario').removeClass('show');
            
            show = false;
        } else {
            $('#bloque').addClass('mini');
            $('#formulario').addClass('show');

            show = true;
        }
    });

    $('.input_maqueta').keypress(function (e) {
        if(e.which ==13){
            $('#btn_login').click();
        }
    });

    $('#btn_login').click(function(){
        $('#btn_login').addClass('noactive');
        let email = $('#email').val();
        let password = $('#password').val();
        
        if(email.length > 0 && password.length > 0) {
            $('#loader').removeClass('hide');

            auth.signInWithEmailAndPassword(email, password)
                .catch(error => {
                    if(!$('#loader').hasClass('hide'))
                        $('#loader').addClass('hide');
                    $('#btn_login').removeClass('noactive');
                    M.toast({html: error.message});
                });
        } else {
            if(!$('#loader').hasClass('hide'))
                $('#loader').addClass('hide');
            $('#btn_login').removeClass('noactive');
            M.toast({html: 'Error, debes llenar todos los campos'});
        }
    });
});