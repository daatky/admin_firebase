
$(document).ready(function(){
    $('.tabs').tabs();
    $('.collapsible').collapsible();

    const api_url = 'http://localhost:4000';
    //const api_url = 'https://newspoth.com';
    var  status_proceso = false;

    var config = {
        apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
        authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
        databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
        projectId: "hotspotmagazine-bf2eb",
        storageBucket: "hotspotmagazine-bf2eb.appspot.com",
        messagingSenderId: "282938551360"
    };
    firebase.initializeApp(config);
    const auth = firebase.auth();

    auth.onAuthStateChanged(function (user) {
        if (user) {
           // console.log(user);
        } else {
            window.location.href = "/";
        }
    });

    function llenarFechaHoy(date) {
        $('.fecha_dia').val(date.getDate());
        $('.fecha_mes').val(date.getMonth() + 1);
        $('.fecha_anno').val(date.getFullYear());
    }

    setInterval(function(){
        let date = new Date();
        let hora = date.getHours();
        let minutos = date.getMinutes();
        let segundos = (date.getSeconds() >= 10) ? date.getSeconds() : '0'+date.getSeconds();

        $('.campo_hora').val(hora+':'+minutos+':'+segundos);
    }, 4000);

    //Evento de cambio en los input del formulario
    //$('#info_empresa').find('input:text').val('');

    function motrarIndicadorProgreso(status){
        if(status) {
            status_proceso = true;
            $('#indicador_progreso').addClass('active');
        } else {
            status_proceso = false;
            $('#indicador_progreso').removeClass('active');
        }
    }

    function motrarIndicadorProgresoID(id, status){
        if(status)
            $('#'+id).addClass('active');
        else
            $('#'+id).removeClass('active');
    }

    function cambiarEstadoBotonModal(id, status){
        if(status) {
            $('#'+id).addClass('noactive');
        } else {
            $('#'+id).removeClass('noactive');
        }
    }

    function mostrarMensajeDeError(jqXHR, textStatus, errorThrown){
        if (jqXHR.status === 0) {
            M.toast({html: 'Error, verifica tu conexión a internet'})
        } else if (jqXHR.status == 404) {
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (jqXHR.status == 500) {
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'parsererror') {
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'timeout') {
            M.toast({html: 'No se ha podido completar la operación'})
        } else if (textStatus === 'abort') {
            M.toast({html: 'No se ha podido completar la operación'})
        } else {
            M.toast({html: 'No se ha podido completar la operación'})
        }
    }

    $('.btn_modal_action').click(function(){
        //Configurar fecha y hora en campos del html
        let date = new Date();
        llenarFechaHoy(date);

        let hora = date.getHours();
        let minutos = date.getMinutes();
        let segundos = (date.getSeconds() >= 10) ? date.getSeconds() : '0'+date.getSeconds();
        $('.campo_hora').val(hora+':'+minutos+':'+segundos);

        let modal = $(this).attr('data-target');
        $('#'+modal).addClass('active');
    });

    $('.btn_close_modale').click(function(){
        let modal = $(this).attr('data-target');
        $('#'+modal).removeClass('active');
        $('#'+modal).find('input').val('');
        $('#'+modal).find('select').val('none');
    });

    function setMesajeToast(msg){
        M.toast({html: msg, classes: 'rounded'});
    }

    function setErrorOInfo(id, msg, tipo){
        $('#'+id).text(msg);
        if(tipo === 0) {
            $('#'+id).removeClass('info');
            $('#'+id).addClass('error');
        } else  if(tipo === 1){
            $('#'+id).removeClass('error');
            $('#'+id).addClass('info');
        } else if(tipo === -1){
            $('#'+id).removeClass('error');
            $('#'+id).removeClass('info');
        }
    }

    function mostrarLoader(id, show){
        if(show) {
            $('#'+id).addClass('active');
        } else {
            $('#'+id).removeClass('active');
        }
    }

    function getURLfromCliente(id){
        let url = '';
        if(id !== 'none') {
            lista_clientes.forEach(c => {
                if(c.id === id) {
                    url = c.data.urls.api_node;
                }
            });
        }
        return url;
    }

    function subirArchivoServidorConRuta($input, url, path, url_api, cb){
        var formData = new FormData(); 
        formData.append('file', $input[0].files[0]);
        formData.append('path', path);

        $.ajax({
            url: url_api + url,
            type:"POST",
            data: formData,
            contentType: false,
            processData: false,
        }).done(function (data) {
            cb(data);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            cb({code:-1, data: 'Error al subir el archivo'});
        });
    }

    function peticionAjaxServidor(d, type, dataType, url, cb) {
        $.ajax({
            url: url,
            type: type,
            cache: false,
            dataType: dataType,
            data: {
                data: JSON.stringify(d)
            }
        }).done(function (d) {
            cb(d);
        }).fail(function (jqXHR, textStatus, errorThrown) {
            mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
            cb({code: -2, data: ''});
        });
    }

    function setValorCampoTipoA(id, val, dt) {
        $('#'+id).val(val);
        $('#'+id).attr('placeholder', val);
        $('#'+id).attr('data-target', dt);
    }

    function setValorCampoTipoB(input, val, dt) {
        $('#'+input).attr('placeholder', val);
        $('#'+input).attr('data-target', dt);
    }

    function updateCampoAnuncio(d){
        lista_anuncios.forEach(ad =>  {
            if(ad.id === d.id_anuncio) {
                if(d.tipo === 0) {
                    ad.data[d.campo] = d.valor;
                } else if(d.tipo === 1){
                    ad.data[d.campo.split('.')[0]][d.campo.split('.')[1]] = d.valor;
                } else if(d.tipo === 2) {
                    ad.data[campo] = JSON.parse(d.valor).info;
                }
            }
        });
    }

    $('.btn_file_modal').click(function(){
        $('#'+$(this).attr('data-input')).click();
    });

    $('.input_file_imagen').change(function(){
        let input = $(this);
        if (this.files && this.files[0]) {
            var reader = new FileReader();
            reader.readAsDataURL(this.files[0]);
            reader.onload = function (e) {
                //Poner la imagen en preview
                let img = '#'+input.attr('data-img');
                $(img).attr('src', e.target.result);
            }
        }
    });

    $('.input_file_video').change(function(){
        let input = $(this);
        if (this.files && this.files[0]) {
            let $source = $('#'+input.attr('data-source'));
            $source[0].src = URL.createObjectURL(this.files[0]);
            $source.parent()[0].load();
        }
    });

    /**
     * Hacia arriba 
     * Común para todos los scripts, revisar ciertos métodos para modificar funcionalidad general
    */

    function updateSelectSubClientePorCliente(tag){
        let id_cliente = tag.val();
        let select = '#'+tag.attr('data-subcliente');
        $(select).empty();
        $(select).attr('disabled', true);
        if(id_cliente != 'none') {
            $(select).append('<option value="none" selected disabled>¿Cuá es el sub-cliente?</option>');
            lista_subclientes.forEach(sub => {
                if(sub.data.id_cliente === id_cliente) {
                    $(select).append('<option value="'+sub.id+'">'+sub.data.etiqueta+'</option>');
                }
            });
            $(select).removeAttr('disabled');
        }
    }

    function updateSelectHotspotsPorCliente(tag){
        let select = tag;
        let cliente = select.val();
        let equipos = '#'+select.attr('data-hotspots');

        $(equipos).empty();
        $(equipos).attr('disabled', true);
        if(cliente !== 'none') {
            $(equipos).append('<option value="none" disabled>¿En que hotspot se mostrara?</option>');
            $(equipos).append('<option value="all">Todos</option>');
            lista_hotspots.forEach( e => {
                if( e.data.id_cliente === cliente ) {
                    $(equipos).append('<option value="'+e.id+'">'+e.data.equipo.ip+' - '+e.data.lugar.nombre+'</option>');
                }
            });
            $(equipos).removeAttr('disabled');
        }
    }

    $('.select_form_cliente').change(function(){
        updateSelectSubClientePorCliente($(this));
        updateSelectHotspotsPorCliente($(this));
    });

    function updateSelectCantonesPorProvincia(tag){
        let select = tag;
        let provincias = select.val();
        let canton = '#'+select.attr('data-canton');

        $(canton).empty();
        $(canton).attr('disabled', true);
        if(provincias.length > 0) {
            $(canton).append('<option value="none" disabled>¿En qué cantón se publicara?</option>');
            let html = '';
            provincias.forEach(p => {
                lista_provincias.forEach(provincia => {
                    if(p === provincia.data.id_provincia) {
                        html += '<optgroup label="'+provincia.data.etiqueta+'">';    
                        lista_cantones.forEach(canton => {
                            if( canton.data.id_provincia === p ) {
                                html += '<option value="'+canton.id+'">'+canton.data.etiqueta+'</option>';
                            }
                        });   
                        html += '</optgroup>';
                    }
                });
            });
            $(canton).append(html);
            $(canton).removeAttr('disabled');
        }
    }

    $('.select_provincia').change(function(){
        updateSelectCantonesPorProvincia($(this));
    });

    $('#reg_ad_tipo').change(function(){
        $('.ad_file_div').addClass('hide');
        switch ($(this).val()) {
            case "0/imagen": 
                $('#reg_ad_preview_imagen').removeClass('hide');
                break;
            case "1/video":
                $('#reg_ad_preview_video').removeClass('hide');
                break;
        }
    });
    
    function validarCamposAnuncio(){
        let res = false;
        if( !($('#reg_ad_id_cliente').val() !== null) ) {
            M.toast({html: 'Error, debes seleccionar el cliente'});  
        } else if( !($('#reg_ad_id_subcliente').val() !== null) ) {
            M.toast({html: 'Error, debes seleccionar el sub-cliente'});
        } else if( !($('#reg_ad_etiqueta').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la etiqueta'});
        } else if( !($('#reg_ad_descripcion').val().length > 0) ) { 
            M.toast({html: 'Error, debes ingresar la descripción'});  
        } else if( !($('#reg_ad_id_provincia').val().length > 0) ) {
            M.toast({html: 'Error, debes seleccionar la provincia o provincias'});  
        } else if( !($('#reg_ad_id_canton').val().length > 0) ) {
            M.toast({html: 'Error, debes seleccionar el canton o cantones'});  
        } else if( !($('#reg_ad_prioridad').val() !== null) ) {
            M.toast({html: 'Error, debes seleccionar la prioridad del anuncio'});
        } else if( !($('#reg_ad_visual_gratis').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la cantidad de visualizaciones gratuitas'});
        } else if( !($('#reg_ad_visual_pagado').val().length > 0) ) {
            M.toast({html: 'Error, debes ingresar la cantidad de visualizaciones pagadas'});
        } else if( !($('#reg_ad_tipo').val() !== null) ) {
            M.toast({html: 'Error, debes seleccionar el tipo de anuncio'});
        } else {
            if( !($('#reg_ad_id_hostspot').val().length > 0) ) {
                M.toast({html: 'Error, debes seleccionar los hotspots'});  
            } else {
                if(($('#reg_ad_id_hostspot').val().indexOf('all') >= 0 && $('#reg_ad_id_hostspot').val().length === 1) || ($('#reg_ad_id_hostspot').val().indexOf('all') < 0 && $('#reg_ad_id_hostspot').val().length >= 1)) {
                    if($('#reg_ad_tipo').val() === '0/imagen') {
                        if($('#reg_ad_file_imagen')[0].files && $('#reg_ad_file_imagen')[0].files[0]) {
                            res = true; 
                        } else {
                            M.toast({html: 'Error, debes seleccionar la imagen para el anuncio'});        
                        }
                    } else if($('#reg_ad_tipo').val() === '1/video'){
                        if($('#reg_ad_file_video')[0].files && $('#reg_ad_file_video')[0].files[0]) {
                            res = true; 
                        } else {
                            M.toast({html: 'Error, debes seleccionar el video para el anuncio'});
                        }
                    }
                } else {
                    M.toast({html: 'Error, en los Hotspots, si seleccionas todos no puedes seleccionar otro equipo'});
                }
            }
        }
        return res;
    }

    function subirArchivoServidor(id_cliente, tipo, cb) {
        let input = '';
        if(tipo === '0/imagen') {
            input = '#reg_ad_file_imagen';
        } else if(tipo === '1/video') {
            input = '#reg_ad_file_video';
        }   

        let name = $(input)[0].files[0].name;
        let url_api = getURLfromCliente(id_cliente);
        subirArchivoServidorConRuta($(input), '/spot/uploadFileAnuncio', 'anuncios/anuncio_name_'+name, url_api, data => {
            cb(data);
        });
    }

    function addAnuncioALista(anuncio){
        let html  = '<li class="li_item" id="li_anuncio_'+anuncio.id+'" data-target="'+anuncio.id+'">';
            html += '   <div class="vertical">';
            html += '       <i class="fas fa-ad"></i>';
            html += '       <span class="maqueta_etiqueta">'+anuncio.data.etiqueta+'</span>';
            html += '   </div>';
            html += '</li>';
        
        $('#ul_anuncios_lista').append(html);
    }

    $('#registrar_anuncio').click(function(){
        if(validarCamposAnuncio()) {
            mostrarLoader('modale_progreso_add_anuncio', true);
            cambiarEstadoBotonModal('registrar_anuncio', true);
            let d = {
                id_cliente: $('#reg_ad_id_cliente').val(),
                id_subcliente: $('#reg_ad_id_subcliente').val(),
                etiqueta: $('#reg_ad_etiqueta').val(),
                descripcion: $('#reg_ad_descripcion').val(),
                fecha: {
                    dia: $('#reg_ad_fecha_dia').val(),
                    mes: $('#reg_ad_fecha_mes').val(),
                    ano: $('#reg_ad_fecha_ano').val(),
                    hora: $('#reg_ad_hora').val(),
                },
                id_provincia: $('#reg_ad_id_provincia').val(),
                id_canton: $('#reg_ad_id_canton').val(),
                prioridad: parseInt($('#reg_ad_prioridad').val()),
                equipos: $('#reg_ad_id_hostspot').val(),
                clicks: {
                    gratis: parseInt($('#reg_ad_visual_gratis').val()),
                    pagados: parseInt($('#reg_ad_visual_pagado').val()),
                    contador: 0
                },
                tipo: $('#reg_ad_tipo').val(),
                url: '',
                display: '0',
            };
            subirArchivoServidor(d.id_cliente, d.tipo, data => {
                if(data.code === 0) {
                    d.url = data.url;
                    peticionAjaxServidor(d, 'POST', 'json', '/admin/registrarAnuncio', data => {
                        M.toast({html: data.data});
                        if(data.code === 0) {
                            $('#modal_add_anuncio').find('input').val('');
                            $('#modal_add_anuncio').find('select').val('none');
                            $('#reg_ad_id_subcliente').empty();
                            $('#reg_ad_id_subcliente').attr('disabled', true);
                            $('#reg_ad_id_canton').empty();
                            $('#reg_ad_id_canton').attr('disabled', true);

                            $('#reg_ad_preview_imagen').find('img').attr('src', '');
                            $('#reg_ad_preview_imagen').addClass('hide');

                            let $source = $('#reg_ad_file_preview_video_source');
                            $source[0].src = '';
                            $source.parent()[0].load();
                            $('#reg_ad_preview_video').addClass('hide');

                            llenarFechaHoy(new Date());

                            lista_anuncios.push(data.anuncio);
                            addAnuncioALista(data.anuncio);

                            mostrarLoader('modale_progreso_add_anuncio', false);
                            cambiarEstadoBotonModal('registrar_anuncio', false);
                        }
                    });
                } else {
                    mostrarLoader('modale_progreso_add_anuncio', false);
                    cambiarEstadoBotonModal('registrar_anuncio', false);
                    if(data.code !== -2){
                        M.toast({html: 'Error al subir el archivo al servidor, anuncio no publicado'});
                    }
                }
            });

        }
    });

    function llenarSubclienteSelect(id_cliente, select){
        $('#'+select).empty();
        $('#'+select).append('<option value="none" selected disabled>¿Cuál es el sub-cliente?</option>');
        lista_subclientes.forEach(sub => {
            if(sub.data.id_cliente === id_cliente) {
                $('#'+select).append('<option value="'+sub.id+'">'+sub.data.etiqueta+'</option>');
            }
        });
    }

    function llenarHotspotsPorCliente(id_cliente, select) {
        $('#'+select).empty();
        $('#'+select).append('<option value="none" disabled>¿En que hotspot se mostrara?</option>');
        $('#'+select).append('<option value="all">Todos</option>');
        lista_hotspots.forEach( e => {
            if(e.data.id_cliente === id_cliente) {
                $('#'+select).append('<option value="'+e.id+'">'+e.data.equipo.ip+' - '+e.data.lugar.nombre+'</option>');
            }
        });
    }

    function llenarCantonesPorPorProvincia(id_provincia, select){
        $('#'+select).empty();
        let html = '<option value="none" selected disabled>En qué cantón se publicara?</option>';
        id_provincia.forEach(p => {
            lista_provincias.forEach(provincia => {
                if(p === provincia.data.id_provincia) {
                    html += '<optgroup label="'+provincia.data.etiqueta+'">';    
                    lista_cantones.forEach(canton => {
                        if( canton.data.id_provincia === p ) {
                            html += '<option value="'+canton.id+'">'+canton.data.etiqueta+'</option>';
                        }
                    });   
                    html += '</optgroup>';
                }
            });
        });
        $('#'+select).append(html);
    }

    function ponerArchivoAnuncioPorTipo(tipo, url, dt) {
        $('.ad_file_div_b').addClass('hide');
        if( tipo === '0/imagen' ) {
            setValorCampoTipoB('ad_file_imagen', url, dt);

            $('#ad_file_preview_imagen').attr('src', url);
            $('#ad_preview_imagen').removeClass('hide');
        } else if ( tipo === '1/video' ){
            setValorCampoTipoB('ad_file_video', url, dt);

            let $source = $('#ad_file_preview_video_source');
            $source[0].src = url;
            $source.parent()[0].load();
            $('#ad_preview_video').removeClass('hide');
        }
    }


    $('body').on('click','.li_item', function(){
        $('.li_item').removeClass('active');
        $(this).addClass('active');
        let id_anuncio = $(this).attr('data-target');
        if(! status_proceso ) {
            motrarIndicadorProgreso(true);
            lista_anuncios.forEach(ad => {
                if(id_anuncio === ad.id) {
                    let { id_cliente, id_subcliente, etiqueta, descripcion, fecha, id_provincia, id_canton, prioridad, equipos, clicks, tipo, url, display  } = ad.data;
                    
                    //Limpiar campos
                    $('#form_anuncio_data').find('input').val('');
                    $('#form_anuncio_data').find('select').val('none');

                    $('#id_anuncio').val(id_anuncio);
                    setValorCampoTipoA('ad_id_cliente', id_cliente, id_anuncio+'|id_cliente|0');
                    llenarSubclienteSelect(id_cliente,'ad_id_subcliente');
                    llenarHotspotsPorCliente(id_cliente,'ad_id_hotspot');
                    setValorCampoTipoA('ad_id_subcliente', id_subcliente, id_anuncio+'|id_subcliente|0');
                    setValorCampoTipoA('ad_etiqueta', etiqueta, id_anuncio+'|etiqueta|0');
                    setValorCampoTipoA('ad_descripcion', descripcion, id_anuncio+'|descripcion|0');
                    setValorCampoTipoA('ad_fecha_dia', fecha.dia, id_anuncio+'|fecha.dia|1');
                    setValorCampoTipoA('ad_fecha_mes', fecha.mes, id_anuncio+'|fecha.mes|1');
                    setValorCampoTipoA('ad_fecha_ano', fecha.ano, id_anuncio+'|fecha.ano|1');
                    setValorCampoTipoA('ad_hora', fecha.hora, id_anuncio+'|fecha.hora|1');
                    
                    setValorCampoTipoA('ad_id_provincia', id_provincia, id_anuncio+'|id_provincia|2');
                    llenarCantonesPorPorProvincia(id_provincia, 'ad_id_canton');
                    setValorCampoTipoA('ad_id_canton', id_canton, id_anuncio+'|id_canton|2');
                    setValorCampoTipoA('ad_id_hotspot', equipos, id_anuncio+'|equipos|2');
                    
                    setValorCampoTipoA('ad_prioridad', prioridad, id_anuncio+'|prioridad|0');

                    setValorCampoTipoA('ad_visual_gratis', clicks.gratis, id_anuncio+'|clicks.gratis|1');
                    setValorCampoTipoA('ad_visual_pagado', clicks.pagados, id_anuncio+'|clicks.pagados|1');
                    setValorCampoTipoA('ad_visual_contador', clicks.contador, id_anuncio+'|clicks.contador|1');

                    setValorCampoTipoA('ad_tipo', tipo, id_anuncio+'|tipo|0');
                    ponerArchivoAnuncioPorTipo(tipo, url, id_anuncio+'|url|0');

                    setValorCampoTipoA('ad_display', display, id_anuncio+'|display|0');

                    motrarIndicadorProgreso(false);
                }
            });
        }else{
            M.toast({html: 'Existe otro proceso en ejecución'});
        }
    });

    function validarCampoSelectHotspot(d){
        if(d.id_campo === 'ad_id_hotspot') {
            let value = JSON.parse(d.valor).info;
            if(value.indexOf('all') >= 0 && value.length > 1) {
                M.toast({html: 'Error, si seleccionas "Todos" no puedes seleccionar mas opciones', displayLength:1600});
                return false;
            } else {
                return true;
            }
        } else {
            return true;
        }
    }

    $('.select_anuncio').change(function(){
        if($('#id_anuncio').val().length > 0) {
            if(!status_proceso) {
                let input = $(this);
                if( (input.val() !== 'none') && (input.val() !== input.attr('placeholder'))) {
                    motrarIndicadorProgreso(true);
                    let d = {
                        id_campo: input.attr('id'),
                        id_anuncio: input.attr('data-target').split('|')[0],
                        campo: input.attr('data-target').split('|')[1],
                        tipo: parseInt(input.attr('data-target').split('|')[2]),
                        old: input.attr('placeholder'),
                        valor: (parseInt(input.attr('data-target').split('|')[2]) === 2) ? JSON.stringify({info: input.val()}) : input.val()
                    };
                    if(validarCampoSelectHotspot(d)) {
                        peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoAnuncio', (data_b) => {
                            if(data_b.code !== -2)
                                M.toast({html: data_b.data, displayLength:1200});

                            if(data_b.code === 0) {
                                updateCampoAnuncio(d);
                                setValorCampoTipoA(d.id_campo, (d.tipo === 2) ? JSON.parse(d.valor).info : d.valor, d.id_anuncio+'|'+d.campo+'|'+d.tipo);
                                if(d.campo === 'id_cliente') {
                                    updateSelectSubClientePorCliente($('#'+d.id_campo));
                                } else if(d.campo === 'id_provincia') {
                                    updateSelectCantonesPorProvincia($('#'+d.id_campo));
                                }
                            }
                            motrarIndicadorProgreso(false);
                        });
                    } else {
                        motrarIndicadorProgreso(false);
                    }
                } else {
                    input.val(input.attr('placeholder'));
                }
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });

    $('.input_anuncio').keypress(function (e) {
		if(e.which ==13){
            if(!status_proceso) {
                if($(this).val().length <= 0){
                    if($(this).attr('data-target').length > 0){
                        $(this).val($(this).attr('placeholder'));
                    }
                } else {
                    if($(this).attr('data-target').length > 0){
                        motrarIndicadorProgreso(true);
                        //Guardar Cambio y actualizar
                        let input = $(this);
                        let d = {
                            id_campo: input.attr('id'),
                            id_anuncio: input.attr('data-target').split('|')[0],
                            campo: input.attr('data-target').split('|')[1],
                            tipo: parseInt(input.attr('data-target').split('|')[2]),
                            old: input.attr('placeholder'),
                            valor: input.val()
                        };
                        peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoAnuncio', (data_b) => {
                            if(data_b.code !== -2)
                                M.toast({html: data_b.data, displayLength:700});

                            if(data_b.code === 0) {
                                updateCampoAnuncio(d);
                                setValorCampoTipoA(d.id_campo, d.valor, d.id_anuncio+'|'+d.campo+'|'+d.tipo);
                            }
                            motrarIndicadorProgreso(false);
                        });
                    }
                }
            }else{
                M.toast({html: 'Existe otro proceso en ejecución', displayLength: 1000});
            }
		}
    });
    
    $('.file_anuncio').change(function(){
        if($('#id_anuncio').val().length > 0) {
            if(!status_proceso) {
                motrarIndicadorProgreso(true);
                let input = $(this);
                let name = $(this)[0].files[0].name;
                let url_api = getURLfromCliente($('#ad_id_cliente').val());
                subirArchivoServidorConRuta(input, '/spot/uploadFileAnuncio', 'anuncios/anuncio_name_'+name, url_api, data => {
                    if(data.code === 0) {
                        let d = {
                            id_campo: input.attr('id'),
                            id_anuncio: input.attr('data-target').split('|')[0],
                            campo: input.attr('data-target').split('|')[1],
                            tipo: parseInt(input.attr('data-target').split('|')[2]),
                            name: input.attr('data-target').split('|')[3],
                            old: input.attr('placeholder'),
                            valor: data.url
                        };
                        peticionAjaxServidor(d, 'POST', 'json', '/admin/updateCampoAnuncio', (data_b) => {
                            if(data_b.code !== -2)
                                M.toast({html: data_b.data});
                            
                            if(data_b.code === 0) {
                                updateCampoAnuncio(d);
                                $('#'+d.id_campo).attr('placeholder', d.valor);
                            }
                            motrarIndicadorProgreso(false);
                        });
                    } else {
                        M.toast({html: 'Error al subir el archivo'});
                    }
                });
            }else{
                M.toast({html: 'Existe otro proceso en ejecución'});
            }
        }
    });
});