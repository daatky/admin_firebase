$(document).ready(function () {
	$('.tabs').tabs();
	$('.collapsible').collapsible();

	var  status_proceso = false;

	var config = {
		apiKey: "AIzaSyDiGS01tMSpsKJachSZzI-NS_ZeIynARqs",
		authDomain: "hotspotmagazine-bf2eb.firebaseapp.com",
		databaseURL: "https://hotspotmagazine-bf2eb.firebaseio.com",
		projectId: "hotspotmagazine-bf2eb",
		storageBucket: "hotspotmagazine-bf2eb.appspot.com",
		messagingSenderId: "282938551360"
	};
	firebase.initializeApp(config);
	const auth = firebase.auth();

	auth.onAuthStateChanged(function (user) {
		if (user) {
			console.log(user);
		} else {
			window.location.href = "/";
		}
	});

	//Evento de cambio en los input del formulario
	//$('#info_empresa').find('input:text').val('');

	function motrarIndicadorProgreso(status){
		if(status) {
			status_proceso = true;
			$('#indicador_progreso').addClass('active');
		} else {
			status_proceso = false;
			$('#indicador_progreso').removeClass('active');
		}
	}

	function motrarIndicadorProgresoID(id, status){
		if(status)
			$('#'+id).addClass('active');
		else
			$('#'+id).removeClass('active');
	}

	function cambiarEstadoBotonModal(id, status){
		if(status) {
			$('#'+id).addClass('noactive');
		} else {
			$('#'+id).removeClass('noactive');
		}
	}

	function mostrarMensajeDeError(jqXHR, textStatus, errorThrown){
		//Validar error y dar control de errores : status
		if (jqXHR.status === 0) {
			//alert('Not connect: Verify Network.');
			M.toast({html: 'Error, verifica tu conexión a internet'})
		} else if (jqXHR.status == 404) {
			//alert('Requested page not found [404]');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (jqXHR.status == 500) {
			//alert('Internal Server Error [500].');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'parsererror') {
			//alert('Requested JSON parse failed.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'timeout') {
			//alert('Time out error.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else if (textStatus === 'abort') {
			//alert('Ajax request aborted.');
			M.toast({html: 'No se ha podido completar la operación'})
		} else {
			//alert('Uncaught Error: ' + jqXHR.responseText);
			M.toast({html: 'No se ha podido completar la operación'})
		}
	}

	function getCliente(id){
		let cliente = null;
		lista_clientes.forEach(function(c){
			if(id === c.id){
				cliente = c;
			}
		});
		return cliente;
	}

	function asignarContadores(cliente) {
		$.ajax({
			url: '/admin/getContadoresPorCliente',
			type: "GET",
			cache: false,
			dataType:'json',
			data: { id: cliente.id }
		}).done(function (data) {
			if(data.code == 0) {
				let iterator = 0;
				let totales = [data.anuncios, data.hotspots, data.plantillas];
				$('.contador').each(function () {
					var $this = $(this);
					let total = totales[iterator];
					iterator += 1;
					$({ counter: 0 }).animate({ counter: ''+total }, {
					  duration: 1500,
					  easing: 'swing',
					  step: function () {
						$this.text(Math.ceil(this.counter));
					  }
					});
				});
				$('#cliente_saldo').text( parseFloat(cliente.data.cuenta.saldo).toFixed(2) );
			} else {
				M.toast({html: data.data})
			}
			motrarIndicadorProgreso(false);
		}).fail(function (jqXHR, textStatus, errorThrown) {
			motrarIndicadorProgreso(false);
			mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
		});
	}

	$('body').on('click', '.li_cliente', function(){
		if(!status_proceso) {
			motrarIndicadorProgreso(true);

			let cliente = getCliente($(this).attr('data-target'));
			console.table(cliente);

			if(cliente != null && !($(this).hasClass('active'))) {
				$('.li_cliente').removeClass('active');
				$(this).addClass('active');

				//limpiar campos data representante y empresa
				$('#info_respresentante').find('input:text').val('');
				$('#info_empresa').find('input:text').val('');
				$('#cliente_anuncios').text('...');
				$('#cliente_hotspots').text('...');
				$('#cliente_plantillas').text('...');
				$('#cliente_saldo').text('...');
				$('#chipes').empty();
				$('#btn_modal_pagos').attr('data-cliente', '');
				$('#reg_pag_id_cliente').val('');

				//Asignar Campos
				$('#nombres').val(cliente.data.nombres);
				$('#nombres').attr('placeholder',cliente.data.nombres);
				$('#nombres').attr('data-target', cliente.id+'|'+'nombres|0');

				$('#apellidos').val(cliente.data.apellidos);
				$('#apellidos').attr('placeholder',cliente.data.apellidos);
				$('#apellidos').attr('data-target', cliente.id+'|'+'apellidos|0');

				$('#email').val(cliente.data.email);
				$('#email').attr('placeholder',cliente.data.email);
				$('#email').attr('data-target', cliente.id+'|'+'email|0');

				$('#telefono').val(cliente.data.telefono);
				$('#telefono').attr('placeholder',cliente.data.telefono);
				$('#telefono').attr('data-target', cliente.id+'|'+'telefono|0');

				$('#empresa_nombre').val(cliente.data.empresa.nombre);
				$('#empresa_nombre').attr('placeholder',cliente.data.empresa.nombre);
				$('#empresa_nombre').attr('data-target', cliente.id+'|'+'empresa.nombre|1');

				$('#empresa_telefono').val(cliente.data.empresa.telefono);
				$('#empresa_telefono').attr('placeholder',cliente.data.empresa.telefono);
				$('#empresa_telefono').attr('data-target', cliente.id+'|'+'empresa.telefono|1');

				$('#empresa_email').val(cliente.data.empresa.email);
				$('#empresa_email').attr('placeholder',cliente.data.empresa.email);
				$('#empresa_email').attr('data-target', cliente.id+'|'+'empresa.email|1');

				$('#empresa_direccion').val(cliente.data.empresa.direccion);
				$('#empresa_direccion').attr('placeholder',cliente.data.empresa.direccion);
				$('#empresa_direccion').attr('data-target', cliente.id+'|'+'empresa.direccion|1');

				$('#estado').val(''+cliente.data.cuenta.estado);
				$('#estado').attr('placeholder',cliente.data.cuenta.estado);
				$('#estado').attr('data-target', cliente.id+'|'+'cuenta.estado|3');
				
				$('#cuenta_licencia').val(cliente.data.cuenta.licencia.tipo+'/'+cliente.data.cuenta.licencia.etiqueta);
				$('#cuenta_licencia').attr('placeholder', cliente.data.cuenta.licencia.tipo+'/'+cliente.data.cuenta.licencia.etiqueta);
				$('#cuenta_licencia').attr('data-target', cliente.id+'|'+'cuenta.licencia|2');

				$('#url_api_node').val(cliente.data.urls.api_node);
				$('#url_api_node').attr('placeholder', cliente.data.urls.api_node);
				$('#url_api_node').attr('data-target', cliente.id+'|'+'urls.api_node|1');

				$('#url_redireccionar').val(cliente.data.urls.url_red);
				$('#url_redireccionar').attr('placeholder', cliente.data.urls.url_red);
				$('#url_redireccionar').attr('data-target', cliente.id+'|'+'urls.url_red|1');

				$('#btn_modal_pagos').attr('data-cliente', cliente.id);
				$('#reg_pag_id_cliente').val(cliente.id);

				//Asignar Sub-Clientes
				lista_subclientes.forEach(sub => {
					if(sub.data.id_cliente === cliente.id) {       
						let html  = '<div class="chipe">';
							html += '	<div class="a">';
							html += '		<i class="fas fa-user-astronaut"></i>';
							html += '	</div>';
							html += '	<div class="b">';
							html += '		<span class="etiqueta">'+sub.data.etiqueta+'</span>';
							html += '		<span class="nombre">'+sub.data.representante+'</span>';
							html += '	</div>';
							html += '</div>';
						$('#chipes').append(html);
					}
				});

				//Asignar contadores
				asignarContadores(cliente);
			} else {
				motrarIndicadorProgreso(false);
			}
		} else {
			M.toast({html: 'Hay otro proceso en ejecución'});
		}
	});
	
	$('.input_usuario').blur(function(){
		let valor = $(this).val();
		if((valor.length <= 0) && ($(this).attr('data-target'))){
			$(this).val($(this).attr('placeholder'));
		}
	});

	$('.input_usuario').keypress(function (e) {
		if(e.which ==13){
			if($(this).val().length <= 0){
				if($(this).attr('data-target').length > 0){
					$(this).val($(this).attr('placeholder'));
				}
			} else {
				if($(this).attr('data-target').length > 0){
					motrarIndicadorProgreso(true);
					//Guardar Cambio y actualizar
					let d = {
						cliente: $(this).attr('data-target').split('|')[0],
						campo: $(this).attr('data-target').split('|')[1],
						tipo: $(this).attr('data-target').split('|')[2],
						old: $(this).attr('placeholder'),
						new: $(this).val(),
					};
					$.ajax({
						url: '/admin/updateCampoCliente',
						type: "POST",
						cache: false,
						dataType:'json',
						data: d
					}).done(function (data) {
						M.toast({html: data.data})
						if(data.code == 0) {
							$('#'+d.campo).attr('placeholder',d.new);
							$('#'+d.campo).val(d.new);

							lista_clientes.forEach(c => {
								if(c.id === d.cliente){
									console.log(c);
									if(d.tipo === '0'){
										c.data[d.campo] = d.new;
									} else if(d.tipo === '1'){
										c.data[d.campo.split('.')[0]][d.campo.split('.')[1]] = d.new;
									}
								}
							});
						}
						motrarIndicadorProgreso(false);
					}).fail(function (jqXHR, textStatus, errorThrown) {
						motrarIndicadorProgreso(false);
						mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
					});
				}
			}
		}
	});

	$('#estado').change(function(){
		if($(this).val() !== 'none') {
			motrarIndicadorProgreso(true);
			let d = {
				cliente: $(this).attr('data-target').split('|')[0],
				campo: $(this).attr('data-target').split('|')[1],
				tipo: $(this).attr('data-target').split('|')[2],
				old: $(this).attr('placeholder'),
				new: parseInt($(this).val()),
			};

			$.ajax({
				url: '/admin/updateCampoCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: d
			}).done(function (data) {
				M.toast({html: data.data})
				if(data.code == 0) {
					$('#estado').attr('placeholder',d.new);
					$('#estado').val(''+d.new);

					lista_clientes.forEach(c => {
						if(c.id === d.cliente){
							c.data.cuenta.estado = d.new;
						}
					});
				}
				motrarIndicadorProgreso(false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgreso(false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			M.toast({html:'Error, debes seleccionar un valor valido'});
			$(this).val($(this).attr('placeholder'));
		}
	});

	$('#cuenta_licencia').change(function(){
		if($(this).val() !== 'none') {
			motrarIndicadorProgreso(true);
			let d = {
				cliente: $(this).attr('data-target').split('|')[0],
				campo: $(this).attr('data-target').split('|')[1],
				tipo: $(this).attr('data-target').split('|')[2],
				old: $(this).attr('placeholder'),
				new: $(this).val(),
			};

			$.ajax({
				url: '/admin/updateCampoCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: d
			}).done(function (data) {
				M.toast({html: data.data})
				if(data.code == 0) {
					$('#cuenta_licencia').attr('placeholder',d.new);
					$('#cuenta_licencia').val(''+d.new);

					lista_clientes.forEach(c => {
						if(c.id === d.cliente){
							c.data.cuenta.licencia.tipo = parseInt(d.new.split('/')[0]);
							c.data.cuenta.licencia.etiqueta = d.new.split('/')[1];
						}
					});
				}
				motrarIndicadorProgreso(false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgreso(false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			M.toast({html:'Error, debes seleccionar un valor valido'});
			$(this).val($(this).attr('placeholder'));
		}
	});

	function cargarHistorialDePagos(id_cliente){
		$('#lista_annos_pagos').empty();
		let lista_annos = [];
		lista_pagos.forEach(pago => {
			if(pago.data.id_cliente === id_cliente) {
				if(lista_annos.indexOf(pago.data.anno)<0) {
					lista_annos.push(pago.data.anno);
				}
			}
		});

		lista_annos.forEach(anno => {                    
			let html  = '<div class="ano">';
				html += '	<span class="anno_pagos_item" data-cliente="'+id_cliente+'" data-anno="'+anno+'">'+anno+'</span>';
				html += '</div>';
			$('#lista_annos_pagos').append(html);
		});
	}

	$('.btn_modal_action').click(function(){
		let modal = $(this).attr('data-target');
		if(modal === 'modal_historial_registro_pagos'){
			if($(this).attr('data-cliente').length > 0) {
				//Cargar historial de pagos antes de mostrar modal
				cargarHistorialDePagos($(this).attr('data-cliente'));
				$('#reg_pag_id_cliente').val($(this).attr('data-cliente'));
				$('#'+modal).addClass('active');
			}
		} else {
			$('#'+modal).addClass('active');
		}
	});

	$('.btn_close_modale').click(function(){
		let modal = $(this).attr('data-target');
		$('#'+modal).removeClass('active');
		$('#'+modal).find('input').val('');
		$('#'+modal).find('select').val('none');
	});

	function validarCamposCliente() {
		let status = false;
		let id_error = 'error_cliente';
		if(! $('#reg_nombres').val().length > 0 ) {
			setError(id_error, 'Error, nombre del representante requerido', 0);
		} else if(! $('#reg_apellidos').val().length > 0 ) {
			setError(id_error, 'Error, apellido del representante requerido', 0);
		 } else if(! $('#reg_email').val().length > 0 ) {
			setError(id_error, 'Error, email del representante requerido', 0);
		} else if(! $('#reg_telefono').val().length > 0 ) {
			setError(id_error, 'Error, teléfono del representante requerido', 0);
		}  else if(! $('#reg_emp_nombre').val().length > 0 ) {
			setError(id_error, 'Error, nombre de la empresa requerido', 0);
		} else if(! $('#reg_emp_direccion').val().length > 0 ) {
			setError(id_error, 'Error, dirección de la empresa requerido', 0);
		} else if(! $('#reg_emp_email').val().length > 0 ) {
			setError(id_error, 'Error, email de la empresa requerido', 0);
		} else if(! $('#reg_emp_telefono').val().length > 0 ) {
			setError(id_error, 'Error, teléfono de la empresa requerido', 0);
		} else if(! ($('#reg_cuenta_tipo').val() !== 'none') ) {
			setError(id_error, 'Error, debes seleccionar el tipo de licencia', 0);
		} else {
			status = true;
		}
		return status;
	}

	function validarCamposSubCliente() {
		let status = false;
		let id_error = '';
		if(! ($('#reg_sub_id_cliente').val() !== 'none') ) {
			setError(id_error, 'Error, el cliente es requerido', 0);
		} else if(! $('#reg_sub_etiqueta').val().length > 0 ) {
			setError(id_error, 'Error, nombre de la empresa requerido', 0);
		 } else if(! $('#reg_sub_representante').val().length > 0 ) {
			setError(id_error, 'Error, nombre del representante requerido', 0);
		} else if(! $('#reg_sub_correo').val().length > 0 ) {
			setError(id_error, 'Error, correo de la empresa o representante requerido', 0);
		} else {
			status = true;
		}
		return status;
	}

	function setError(id, msg, opc){
		M.toast({html: msg, classes: 'rounded'});
		/*if(opc === 1) { //Mensaje
			$('#'+id).removeClass('error');
			$('#'+id).addClass('msg');
		} else  if(opc === 0){ //Error
			$('#'+id).removeClass('msg');
			$('#'+id).addClass('error');
		}*/
	}

	$('#registrar_cliente').click(function(){
		motrarIndicadorProgresoID('modale_progreso_cliente', true);
		cambiarEstadoBotonModal('registrar_cliente', true);
		if(validarCamposCliente()){
			let d = {
				nombres: $('#reg_nombres').val(),
				apellidos: $('#reg_apellidos').val(),
				telefono: $('#reg_telefono').val(),
				email: $('#reg_email').val(),
				empresa: {
					nombre: $('#reg_emp_nombre').val(),
					direccion: $('#reg_emp_direccion').val(),
					email: $('#reg_emp_email').val(),
					telefono: $('#reg_emp_telefono').val(),
				},
				cuenta: {
					estado: 0,
					licencia: {
						tipo: parseInt($('#reg_cuenta_tipo').val().split('/')[0]),
						etiqueta: $('#reg_cuenta_tipo').val().split('/')[1],
					},
					saldo: ($('#reg_cuenta_saldo').val().length > 0) ? parseInt($('#reg_cuenta_saldo').val()) : 0
				},
				tipo: 1,
				urls: {
					api_node: $('#reg_url_api_node').val(),
					url_red: $('#reg_url_redireccionar').val()
				}
			};
			$.ajax({
				url: '/admin/registrarCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				setError('error_cliente', data.data, 0);
				if(data.code == 0) {
					lista_clientes.push(data.cliente);
					$('#reg_sub_id_cliente').append('<option value="'+data.cliente.id+'">'+data.cliente.data.empresa.nombre+'</option>');
					let html  = '<li class="li_cliente" id="li_clientes_'+data.cliente.id+'" data-target="'+data.cliente.id+'">';
						html += '	<div class="vertical">';
						html += '		<i class="fas fa-briefcase"></i>';
						html += '		<span class="cliente_nombre">'+data.cliente.data.empresa.nombre+'</span>';
						html += '	</div>';
						html += '</li>';
					$('#ul_clientes_lista').append(html);
					$('#modal_add_cliente').find('input').val('');
					$('#modal_add_cliente').find('select').val('none');
				}
				motrarIndicadorProgresoID('modale_progreso_cliente', false);
				cambiarEstadoBotonModal('registrar_cliente', false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgresoID('modale_progreso_cliente', false);
				cambiarEstadoBotonModal('registrar_cliente', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			motrarIndicadorProgresoID('modale_progreso_cliente', false);
			cambiarEstadoBotonModal('registrar_cliente', false);
		}
	});

	$('#registrar_subcliente').click(function(){
		motrarIndicadorProgresoID('modale_progreso_subcliente', true);
		cambiarEstadoBotonModal('registrar_subcliente', true);
		if(validarCamposSubCliente()){
			let d = {
				id_cliente: $('#reg_sub_id_cliente').val(),
				etiqueta: $('#reg_sub_etiqueta').val(),
				representante: $('#reg_sub_representante').val(),
				correo: $('#reg_sub_correo').val(),
				estado: 0,
			};
			$.ajax({
				url: '/admin/registrarSubCliente',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				setError('', data.data, 0);
				if(data.code == 0) {
					lista_subclientes.push(data.subcliente);
					$('#modal_add_sub_cliente').find('input').val('');
					$('#modal_add_sub_cliente').find('select').val('none');
				}
				motrarIndicadorProgresoID('modale_progreso_subcliente', false);
				cambiarEstadoBotonModal('registrar_subcliente', false);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				motrarIndicadorProgresoID('modale_progreso_subcliente', false);
				cambiarEstadoBotonModal('registrar_subcliente', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			motrarIndicadorProgresoID('modale_progreso_subcliente', false);
			cambiarEstadoBotonModal('registrar_subcliente', false);
		}
	});

	function setErrorOInfo(id, msg, tipo){
		$('#'+id).text(msg);
		if(tipo === 0) {
			$('#'+id).removeClass('info');
			$('#'+id).addClass('error');
		} else  if(tipo === 1){
			$('#'+id).removeClass('error');
			$('#'+id).addClass('info');
		} else if(tipo === -1){
			$('#'+id).removeClass('error');
			$('#'+id).removeClass('info');
		}
	}

	function mostrarLoader(id, show){
		if(show) {
			$('#'+id).addClass('active');
		} else {
			$('#'+id).removeClass('active');
		}
	}

	function validarCamposUbicaciones(tipo){
		let res = false;
		if(tipo === 1) {
			//Provincia
			let id = 'error_provincia';
			if(!($('#reg_pro_id_provincia').val().length > 0)) {
				setErrorOInfo(id, 'Error, debes ingresar el código de la provincia', 0);
			} else if(!($('#reg_pro_etiqueta').val().length > 0)){
				setErrorOInfo(id, 'Error, debes ingresar el nombre de la provincia', 0);
			} else {
				setErrorOInfo(id, '', -1);
				res = true;
			}
		} else {
			//Cantón
			let id = 'error_canton';
			if(!($('#reg_can_id_provincia').val() !== 'none')) {
				setErrorOInfo(id, 'Error, debes seleccionar la provincia', 0);
			} else if(!($('#reg_can_etiqueta').val().length > 0)){
				setErrorOInfo(id, 'Error, debes ingresar el nombre del cantón', 0);
			} else {
				setErrorOInfo(id, '', -1);
				res = true;
			}
		}
		return res;
	}

	function insertarProvinciaALista(provincia) {
		let html  = '<li id="bloque_collap_'+provincia.id+'">';
			html += '	<div class="collapsible-header">';
			html += '		<i class="fas fa-city"></i>';
			html += '		'+provincia.data.etiqueta;
			html += '	</div>';
			html += '	<div class="collapsible-body" id="div_pro_can_'+provincia.data.id_provincia+'">';
			html += '		<span id="eliminar_provincia_'+provincia.id+'" data-target="'+provincia.id+'|'+provincia.data.id_provincia+'" class="btn_eliminar_provincia">Eliminar provincia</span>';
			html += '		<ul class="collection" id="lista_pro_can_'+provincia.data.id_provincia+'">';
			html += '		</ul>';
			html += '	</div>';
			html += '</li>';
		$('#lista_collap_provincias').append(html);
		$('#lista_collap_provincias').collapsible();

		$('#reg_can_id_provincia').append('<option value="'+provincia.data.id_provincia+'">'+provincia.data.etiqueta+'</option>');
	}

	$('#registrar_provincia').click(function(){
		mostrarLoader('loader_provincia', true);
		cambiarEstadoBotonModal('registrar_provincia', true);
		if(validarCamposUbicaciones(1)) {
			let d = {
				id_provincia: $('#reg_pro_id_provincia').val(),
				etiqueta: $('#reg_pro_etiqueta').val(),
			};
			$.ajax({
				url: '/admin/registrarProvincia',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				if(data.code == 0) {
					lista_provincias.push(data.provincia);
					insertarProvinciaALista(data.provincia);
					setErrorOInfo('error_provincia', data.data, 1);
					$('#reg_pro_id_provincia').val('');
					$('#reg_pro_etiqueta').val('');
				} else {
					setErrorOInfo('error_provincia', data.data, 0);
				}
				mostrarLoader('loader_provincia', false);
				setTimeout(()=>{
					setErrorOInfo('error_provincia', '', -1);
					cambiarEstadoBotonModal('registrar_provincia', false);
				}, 1500);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				mostrarLoader('loader_provincia', false);
				cambiarEstadoBotonModal('registrar_provincia', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			mostrarLoader('loader_provincia', false);
			cambiarEstadoBotonModal('registrar_provincia', false);
		}
	});

	function insertarCantonALista(canton) {
		let html  = '<li class="collection-item" id="item_pro_can_'+canton.id+'">';
			html += '	'+canton.data.etiqueta;
			html += '	<span class="eliminar_canton" data-target="'+canton.id+'"></span>';
			html += '</li>';
		$('#lista_pro_can_'+canton.data.id_provincia).append(html);
	}

	$('#registrar_canton').click(function(){
		mostrarLoader('loader_canton', true);
		cambiarEstadoBotonModal('registrar_canton', true);
		if(validarCamposUbicaciones(2)) {
			let d = {
				id_provincia: $('#reg_can_id_provincia').val(),
				etiqueta: $('#reg_can_etiqueta').val(),
			};
			$.ajax({
				url: '/admin/registrarCanton',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				if(data.code == 0) {
					lista_cantones.push(data.canton);
					insertarCantonALista(data.canton);
					setErrorOInfo('error_canton', data.data, 1);
					$('#reg_can_id_provincia').val('none');
					$('#reg_can_etiqueta').val('');
				} else {
					setErrorOInfo('error_canton', data.data, 0);
				}
				mostrarLoader('loader_canton', false);
				setTimeout(()=>{
					setErrorOInfo('error_canton', '', -1);
					cambiarEstadoBotonModal('registrar_canton', false);
				}, 1500);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				mostrarLoader('loader_canton', false);
				cambiarEstadoBotonModal('registrar_canton', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			mostrarLoader('loader_canton', false);
			cambiarEstadoBotonModal('registrar_canton', false);
		}
	});

	$('body').on('click', '.btn_eliminar_provincia', function(){
		let id = $(this).attr('data-target').split('|')[0];
		let codigo = $(this).attr('data-target').split('|')[1];
		cambiarEstadoBotonModal('eliminar_provincia_'+id, true);
		$.ajax({
			url: '/admin/eliminarProvincia',
			type: "POST",
			cache: false,
			dataType:'json',
			data: {
				id_provincia: id,
				codigo: codigo,
			}
		}).done(function (data) {
			M.toast({html: data.data})
			if(data.code == 0) {
				$('#bloque_collap_'+id).remove();
				$('#reg_can_id_provincia option[value="'+codigo+'"]').remove();
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			cambiarEstadoBotonModal('eliminar_provincia_'+id, true);
			mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
		});
	});

	$('body').on('click', '.eliminar_canton', function(){
		let id = $(this).attr('data-target');
		$.ajax({
			url: '/admin/eliminarCanton',
			type: "POST",
			cache: false,
			dataType:'json',
			data: {
				id_canton: id,
			}
		}).done(function (data) {
			M.toast({html: data.data})
			if(data.code == 0) {
				$('#item_pro_can_'+id).remove();
			}
		}).fail(function (jqXHR, textStatus, errorThrown) {
			mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
		});
	});

	$('#btn_modal_pagos').click(function(){
		let cliente = $(this).attr('data-target');
		if(cliente.length > 0){
			//Obtener Historial de Pagos del cliente
		}
	});

	function validarCamposPago() {
		let status = false;
		let id_error = 'error_pago';
		if(! $('#reg_pag_descripcion').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, descripción del pago requqerida', 0);
		} else if(! ($('#reg_pag_medio').val()  !== 'none' )) {
			setErrorOInfo(id_error, 'Error, medio de pago invalido', 0);
		 } else if(! $('#reg_pago_nro_comprobante').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, número de comprobante o factura requerido', 0);
		} else if(! $('#reg_pag_fecha_dia').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, la fecha es invalida', 0);
		}  else if(! $('#reg_pag_fecha_mes').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, la fecha es invalida', 0);
		} else if(! $('#reg_pag_fecha_ano').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, la fecha es invalida', 0);
		} else if(! $('#reg_pag_monto').val().length > 0 ) {
			setErrorOInfo(id_error, 'Error, el monto del pago es requerido', 0);
		} else {
			setErrorOInfo(id_error, '', -1);
			status = true;
		}
		return status;
	}

	$('#reg_pago_action').click(function(){
		mostrarLoader('loader_pago', true);
		cambiarEstadoBotonModal('reg_pago_action', true);
		if(validarCamposPago()) {
			let d = {
				tipo: 0, //0 Ingreso, 1 Egreso
				id_cliente: $('#reg_pag_id_cliente').val(),
				saldoa_anterior: 0,
				descripcion: $('#reg_pag_descripcion').val(),
				medio: {
					codigo: $('#reg_pag_medio').val().split('/')[0],
					etiqueta: $('#reg_pag_medio').val().split('/')[1],
				},
				nro_comprobante: $('#reg_pago_nro_comprobante').val(),
				dia: parseInt($('#reg_pag_fecha_dia').val()),
				mes: parseInt($('#reg_pag_fecha_mes').val()),
				anno: parseInt($('#reg_pag_fecha_ano').val()),
				monto: parseFloat($('#reg_pag_monto').val()),
			};
			$.ajax({
				url: '/admin/registrarPago',
				type: "POST",
				cache: false,
				dataType:'json',
				data: {
					data: JSON.stringify(d)
				}
			}).done(function (data) {
				if(data.code == 0) {
					lista_pagos.push(data.pago);
					setErrorOInfo('error_pago', data.data, 1);

					$('#reg_pag_descripcion').val('');
					$('#reg_pag_medio').val('none');
					$('#reg_pago_nro_comprobante').val('');
					$('#reg_pag_fecha_dia').val('');
					$('#reg_pag_fecha_mes').val('');
					$('#reg_pag_fecha_ano').val('');
					$('#reg_pag_monto').val('');

					lista_clientes.forEach(c => {
						if(c.id === d.id_cliente){
							$({ counter: c.data.cuenta.saldo }).animate({ counter: ''+data.saldo_new }, {
								duration: 1500,
								easing: 'swing',
								step: function () {
								  $('#cliente_saldo').text(Math.ceil(this.counter));
								}
							});
							c.data.cuenta.saldo = data.saldo_new;
						}
					});
				} else {
					setErrorOInfo('error_pago', data.data, 0);
				}
				mostrarLoader('loader_pago', false);
				setTimeout(()=>{
					setErrorOInfo('error_pago', '', -1);
					cambiarEstadoBotonModal('reg_pago_action', false);
				}, 1500);
			}).fail(function (jqXHR, textStatus, errorThrown) {
				mostrarLoader('loader_pago', false);
				cambiarEstadoBotonModal('reg_pago_action', false);
				mostrarMensajeDeError(jqXHR, textStatus, errorThrown);
			});
		} else {
			mostrarLoader('loader_pago', false);
			cambiarEstadoBotonModal('reg_pago_action', false);
		}
	});

	function getDataMes(mes){
		if(mes === 1) {
			return {
				mes: mes,
				etiqueta: 'Enero'
			};
		} else if(mes === 2) {
			return {
				mes: mes,
				etiqueta: 'Febrero'
			};
		} else if(mes === 3) {
			return {
				mes: mes,
				etiqueta: 'Marzo'
			};
		} else if(mes === 4) {
			return {
				mes: mes,
				etiqueta: 'Abril'
			};
		} else if(mes === 5) {
			return {
				mes: mes,
				etiqueta: 'Mayo'
			};
		} else if(mes === 6) {
			return {
				mes: mes,
				etiqueta: 'Junio'
			};
		} else if(mes === 7) {
			return {
				mes: mes,
				etiqueta: 'Julio'
			};
		} else if(mes === 8) {
			return {
				mes: mes,
				etiqueta: 'Agosto'
			};
		} else if(mes === 9) {
			return {
				mes: mes,
				etiqueta: 'Septiembre'
			};
		} else if(mes === 10) {
			return {
				mes: mes,
				etiqueta: 'Octubre'
			};
		} else if(mes === 11) {
			return {
				mes: mes,
				etiqueta: 'Noviembre'
			};
		} else if(mes === 12) {
			return {
				mes: mes,
				etiqueta: 'Diciembre'
			};
		}
	}

	function obtenerListaMeses(cliente, anno){
		let meses = [];
		let n = [];
		
		lista_pagos.forEach(pago => {
			if(pago.data.id_cliente === cliente && pago.data.anno === anno) {
				if(n.indexOf(pago.data.mes) < 0) {
					n.push(pago.data.mes);
					meses.push(getDataMes(pago.data.mes));
				}
			}
		});

		for (let i = 0; i<meses.length; i++){
			for (let j = 0; j<meses.length; j++){
				let aux;
				if(meses[i].mes < meses[j].mes) {
					aux = meses[i];
					meses[i] = meses[j];
					meses[j] = aux;
				}
			}
		}
		
		return meses;
	}

	$('body').on('click', '.anno_pagos_item', function(){
		$('#div_pagos_por_anno').empty();
		let cliente = $(this).attr('data-cliente');
		let anno = parseInt($(this).attr('data-anno'));
		$('.anno_pagos_item').removeClass('active');
		$(this).addClass('active');
		
		let lista_meses = obtenerListaMeses(cliente, anno);
		lista_meses.forEach(mes => {
			let html  = '<li>';
				html += '	<div class="collapsible-header">';
				html += '		<i class="fas fa-calendar-alt"></i>';
				html += '		'+mes.etiqueta;
				html += '	</div>';
				html += '	<div class="collapsible-body">';
				html += '		<table>';
				html += '			<thead>';
				html += '				<tr>';
				html += '					<td>Medio</td>';
				html += '					<td>Comprobante</td>';
				html += '					<td>Fecha</td>';
				html += '					<td>Monto</td>';
				html += '				</tr>';
				html += '			</thead>';
				html += '			<tbody>';
				lista_pagos.forEach(pago => {
					if(pago.data.id_cliente === cliente && pago.data.anno === anno && pago.data.mes === mes.mes){
						html += '	<tr>';
						html += '		<td>'+pago.data.medio.etiqueta+'</td>';
						html += '		<td>'+pago.data.nro_comprobante+'</td>';
						html += '		<td>'+pago.data.dia+'-'+pago.data.mes+'-'+pago.data.anno+'</td>';
						html += '		<td>'+pago.data.monto+'</td>';
						html += '	</tr>';
					}
				});
				html += '			</tbody>';
				html += '		</table>';
				html += '	</div>';
				html += '</li>';
			
			$('#div_pagos_por_anno').append(html);
			$('#div_pagos_por_anno').collapsible();
		});
	});
});